<?php

namespace App\Http\Controllers;

use App\Models\Fakultas;
use App\Models\Kelas;
use App\Models\Mentee;
use App\Models\TahunAjar;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{
    public function laporanMentor()
    {
        $tahun_ajar = TahunAjar::where('active', true)->first();
        $mentor = Auth::user();
        $kelas = Kelas::where('tahun_ajar_id', $tahun_ajar->id)
            ->where('mentor_id', $mentor->id)->first();

        $data = Mentee::with([
            'absensi' => function (Builder $query) {
                $query->orderBy('pertemuan', 'asc');
            },
            'nilai' => function (Builder $query) {
                $query->orderBy('pertemuan', 'asc');
            },
        ])->where('kelas_id', $kelas->id)->get();
        return view('mentor.laporan.index', compact('kelas', 'data'));
    }

    public function laporanFakultas()
    {
        $tahun_ajar = TahunAjar::where('active', true)->first();
        $kmf = Auth::user();
        $fakultas = Fakultas::find($kmf->fakultas->id);

        // dd($fakultas->kelas);

        $data = Mentee::with([
            'absensi' => function (Builder $query) {
                $query->orderBy('pertemuan', 'asc');
            },
            'nilai' => function (Builder $query) {
                $query->orderBy('pertemuan', 'asc');
            },
        ])->get();
        return view('kmf.laporan.index', compact('fakultas'));
    }

    public function laporanFilterFakultas(Request $request)
    {
        $kelas = Kelas::find($request->kelas_id);
        $jenis = $request->jenis;
        $data = Mentee::with([
            'absensi' => function (Builder $query) {
                $query->orderBy('pertemuan', 'asc');
            },
            'nilai' => function (Builder $query) {
                $query->orderBy('pertemuan', 'asc');
            },
        ])->where('kelas_id', $kelas->id)->get();
        return view('kmf.laporan.table_data', compact('kelas', 'jenis', 'data'));
    }
}
