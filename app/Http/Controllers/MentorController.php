<?php

namespace App\Http\Controllers;

use App\Models\Fakultas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MentorController extends Controller
{
    public function index(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = User::where('role', 'mentor')->get();
            return view('kmp.mentor.table_data', compact('data'));
        }
        $fakultas = Fakultas::orderBy('nama')->get();
        return view('kmp.mentor.index', compact('fakultas'));
    }

    public function kmfIndex(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = User::where('role', 'mentor')->where('fakultas_id', Auth::user()->fakultas_id)->orderBy('nama')->get();
            return view('kmf.mentor.table_data', compact('data'));
        }
        return view('kmf.mentor.index');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fakultas_id' => 'required',
            'nama' => 'required',
            'username' => [
                'required',
                'alpha_dash:ascii',
                Rule::unique('users', 'username')->where(function ($query) {
                    return $query->where('role', 'mentor');
                }),
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->where(function ($query) {
                    return $query->where('role', 'mentor');
                }),
            ],
            'fakultas_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Add data to database and send response
        User::create([
            'fakultas_id' => $request->fakultas_id,
            'nama' => ucwords($request->nama),
            'username' => strtolower($request->username),
            'email' => $request->email,
            'password' => bcrypt('password'),
            'role' => 'mentor',
        ]);
        return $this->notifyResponse('success', 'Submit succeeded', 'Your data has been successfully submited.');
    }

    public function show(User $mentor)
    {
        return response()->json($mentor);
    }

    public function update(Request $request, User $mentor)
    {
        $validator = Validator::make($request->all(), [
            'fakultas_id' => 'required',
            'nama' => 'required',
            'username' => [
                'required',
                'alpha_dash:ascii',
                Rule::unique('users', 'username')->ignore($mentor->id)->where(function ($query) {
                    return $query->where('role', 'mentor');
                }),
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($mentor->id)->where(function ($query) {
                    return $query->where('role', 'mentor');
                }),
            ],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Update data to database and send response
        $mentor->update([
            'nama' => ucwords($request->nama),
            'username' => strtolower($request->username),
            'email' => $request->email,
        ]);
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    public function destroy(User $mentor)
    {
        $mentor->delete();
        return $this->notifyResponse('success', 'Delete succeeded', 'Your data has been successfully deleted.');
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }
}
