<?php

namespace App\Http\Controllers;

use App\Models\Fakultas;
use App\Models\Kelas;
use App\Models\Mentee;
use App\Models\TahunAjar;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class KelasController extends Controller
{
    public function index(Request $request)
    {
        $tahun_ajar = TahunAjar::where('active', true)->first();
        $fakultas = Fakultas::find(Auth::user()->fakultas_id);
        $mentor = User::where('role', 'mentor')
            ->where('fakultas_id', $fakultas->id)
            ->orderBy('nama')->get();
        if ($request->option == 'load_table_data') {
            $data = Kelas::where('fakultas_id', $fakultas->id)->get();
            return view('kmf.kelas.table_data', compact('data'));
        }
        return view('kmf.kelas.index', compact('tahun_ajar', 'fakultas', 'mentor'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'mentor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Add data to database and send response
        Kelas::create([
            'tahun_ajar_id' => $request->tahun_ajar_id,
            'fakultas_id' => $request->fakultas_id,
            'tipe' => $request->tipe,
            'mentor_id' => $request->mentor_id,
            'nama' => $request->nama,
        ]);
        return $this->notifyResponse('success', 'Submit succeeded', 'Your data has been successfully submited.');
    }

    public function show(Kelas $kelas)
    {
        return response()->json($kelas);
    }

    public function edit(Request $request, Kelas $kelas)
    {
        if ($request->option == 'load_table_data') {
            $data = Mentee::where('kelas_id', $kelas->id)->get();
            return view('kmf.kelas.table_data_edit', compact('data'));
        }
        if ($request->option == 'load_mentee') {
            $mentee = Mentee::where('fakultas_id', $kelas->fakultas_id)->where('kelas_id',  null)->get();
            return response()->json($mentee);
        }
        $mentee = Mentee::where('fakultas_id', $kelas->fakultas_id)->where('kelas_id',  null)->get();
        return view('kmf.kelas.edit', compact('kelas', 'mentee'));
    }

    public function update(Request $request, Kelas $kelas)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'mentor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Update data to database and send response
        $kelas->update([
            'nama' => $request->nama,
            'tipe' => $request->tipe,
            'mentor_id' => $request->mentor_id,
        ]);
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    public function destroy(Kelas $kelas)
    {
        $kelas->delete();
        return $this->notifyResponse('success', 'Delete succeeded', 'Your data has been successfully deleted.');
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }

    public function add_mentee_to_kelas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mentee_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        // Update data to database and send response
        $mentee = Mentee::find($request->mentee_id);
        $mentee->update([
            'kelas_id' => $request->kelas_id,
        ]);
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    public function remove_mentee_from_kelas(Request $request)
    {
        $mentee = Mentee::find($request->mentee_id);
        $mentee->update([
            'kelas_id' => null,
        ]);
        return $this->notifyResponse('success', 'Delete succeeded', 'Your data has been successfully deleted.');
    }
}
