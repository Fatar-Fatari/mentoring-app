<?php

namespace App\Http\Controllers;

use App\Models\TahunAjar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TahunAjarController extends Controller
{
    public function index(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = TahunAjar::orderBy('start', 'desc')->get();
            return view('kmp.tahun_ajar.table_data', compact('data'));
        }
        if ($request->option == 'load_select_option') {
            $data = TahunAjar::orderBy('start', 'desc')->get();
            return response()->json($data);
        }
        return view('kmp.tahun_ajar.index');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tahun_ajar' => 'required',
            'start' => 'required',
            'end' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Add data to database and send response
        try {
            TahunAjar::create([
                'tahun_ajar' => $request->tahun_ajar,
                'start' => $request->start,
                'end' => $request->end,
            ]);
            return view('layouts.partials.notify', [
                'status' => 'success',
                'title' => 'Submit succedeed',
                'message' => 'Your data has been successfully submitd.',
            ]);
        } catch (\Throwable $th) {
            return view('layouts.partials.notify', [
                'status' => 'failed',
                'title' => 'Submit failed',
                'message' => 'Something wrong. The submit cannot be processed.',
            ]);
        }
    }

    public function show(TahunAjar $tahunAjar)
    {
        return response()->json($tahunAjar);
    }

    public function update(Request $request, TahunAjar $tahunAjar)
    {
        $validator = Validator::make($request->all(), [
            'tahun_ajar' => 'required',
            'start' => 'required',
            'end' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Update data to database and send response
        try {
            $tahunAjar->update([
                'tahun_ajar' => $request->tahun_ajar,
                'start' => $request->start,
                'end' => $request->end,
            ]);
            return view('layouts.partials.notify', [
                'status' => 'success',
                'title' => 'Update succedeed',
                'message' => 'Your data has been successfully updated.',
            ]);
        } catch (\Throwable $th) {
            return view('layouts.partials.notify', [
                'status' => 'failed',
                'title' => 'Update failed',
                'message' => 'Something wrong. The update cannot be processed.',
            ]);
        }
    }

    public function destroy(TahunAjar $tahunAjar)
    {
        try {
            $tahunAjar->delete();
            return view('layouts.partials.notify', [
                'status' => 'success',
                'title' => 'Delete succedeed',
                'message' => 'Your data has been successfully deleted.',
            ]);
        } catch (\Throwable $th) {
            return view('layouts.partials.notify', [
                'status' => 'failed',
                'title' => 'Delete failed',
                'message' => 'Something wrong. The delete cannot be processed.',
            ]);
        }
    }

    public function update_active(Request $request)
    {
        try {
            TahunAjar::query()->update([
                'active' => false
            ]);
            TahunAjar::where('id', $request->id)->update([
                'active' => true
            ]);
            return view('layouts.partials.notify', [
                'status' => 'success',
                'title' => 'Upated succedeed',
                'message' => 'New active data has been set.',
            ]);
        } catch (\Throwable $th) {
            return view('layouts.partials.notify', [
                'status' => 'failed',
                'title' => 'Update failed',
                'message' => 'Something wrong. The update cannot be processed.',
            ]);
        }
    }
}
