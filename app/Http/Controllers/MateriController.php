<?php

namespace App\Http\Controllers;

use App\Models\Materi;
use App\Models\TahunAjar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class MateriController extends Controller
{
    public function index(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = Materi::all();
            return view('kmp.materi.table_data', compact('data'));
        }
        $tahun_ajar = TahunAjar::where('active', true)->first();
        return view('kmp.materi.index', compact('tahun_ajar'));
    }

    public function mentorIndex(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = Materi::all();
            return view('mentor.materi.table_data', compact('data'));
        }
        $tahun_ajar = TahunAjar::where('active', true)->first();
        return view('mentor.materi.index', compact('tahun_ajar'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'file' => 'required|mimes:pdf,doc,docx,rtf'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Add file to directory
        if ($request->file('file')) {
            $file = $request->file('file');
            $filename = time() . $file->getClientOriginalName();
            $file->move(public_path('file-uploads/materi'), $filename);
        }
        // Add data to database and send response
        Materi::create([
            'tahun_ajar_id' => $request->tahun_ajar_id,
            'nama' => ucwords($request->nama),
            'file' => $filename,
        ]);
        return $this->notifyResponse('success', 'Submit succeeded', 'Your data has been successfully submited.');
    }

    public function show(Materi $materi)
    {
        return response()->json($materi);
    }

    public function update(Request $request, Materi $materi)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Update the file
        if ($request->file('file')) {
            if (File::exists(public_path('file-uploads/meteri') . '/' . $materi->file)) {
                File::delete(public_path('file-uploads/meteri') . '/' . $materi->file);
            }
            $file = $request->file('file');
            $filename = time() . $file->getClientOriginalName();
            $file->move(public_path('file-uploads/materi'), $filename);
        }
        // Update data to database and send response
        $materi->update([
            'nama' => ucwords($request->nama),
            'file' => $filename,
        ]);
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    public function destroy(Materi $materi)
    {
        $materi->delete();
        return $this->notifyResponse('success', 'Delete succeeded', 'Your data has been successfully deleted.');
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }
}
