<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Mentee;
use App\Models\Nilai;
use App\Models\TahunAjar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NilaiController extends Controller
{
    public function index(Request $request)
    {
        $tahun_ajar = TahunAjar::where('active', true)->first();
        $mentor = Auth::user();
        $kelas = Kelas::where('tahun_ajar_id', $tahun_ajar->id)
            ->where('mentor_id', $mentor->id)->first();

        if ($request->option == 'load_table_data') {
            $data = Mentee::select('mentees.*', 'nilais.id as nilai_id', 'nilais.nilai')
                ->leftJoin('nilais', 'nilais.mentee_id', '=', 'mentees.id')
                ->where('mentees.kelas_id', $kelas->id)
                ->where('nilais.kelas_id', $kelas->id)
                ->where('nilais.pertemuan', $request->pertemuan)
                ->get();
            if ($data->isEmpty()) {
                $this->_setup_nilai($kelas->id, $request->pertemuan);
                $data = Mentee::select('mentees.*', 'nilais.id as nilai_id', 'nilais.nilai')
                    ->leftJoin('nilais', 'nilais.mentee_id', '=', 'mentees.id')
                    ->where('mentees.kelas_id', $kelas->id)
                    ->where('nilais.kelas_id', $kelas->id)
                    ->where('nilais.pertemuan', $request->pertemuan)
                    ->get();
            }
            return view('mentor.nilai.table_data', compact('data'));
        }

        return view('mentor.nilai.index', compact('kelas'));
        //
    }

    public function store(Request $request)
    {
        $data = array_combine($request->nilai_id, $request->nilai);

        foreach ($data as $key => $value) {
            try {
                Nilai::find($key)->update(['nilai' => intval($value)]);
            } catch (\Throwable $th) {
                dd($th);
            }
        }
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }

    private function _setup_nilai($kelas_id, $pertemuan)
    {
        for ($i = 1; $i <= 14; $i++) {
            $data = Mentee::where('mentees.kelas_id', $kelas_id)
                ->get();
            foreach ($data as $item) {
                Nilai::create([
                    'kelas_id' => $kelas_id,
                    'mentee_id' => $item->id,
                    'pertemuan' => $i,
                    'nilai' => 0,
                ]);
            }
        }
    }
}
