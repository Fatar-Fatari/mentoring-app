<?php

namespace App\Http\Controllers;

use App\Imports\MenteesImport;
use App\Imports\MentorImport;
use App\Imports\PengurusImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function pengurus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $file = $request->file('file');
        $filename = $file->hashName();
        $file->move(public_path('file-uploads'), $filename);
        try {
            Excel::import(new PengurusImport(), public_path('file-uploads') . '/' . $filename);
            File::delete(public_path('file-uploads') . '/' . $filename);
            return $this->notifyResponse('success', 'Upload succeeded', 'Your data has been successfully uploaded.');
        } catch (\Throwable $th) {
            File::delete(public_path('file-uploads') . '/' . $filename);
            return $this->notifyResponse('failed', 'Upload failed', $th->getMessage());
        }
    }

    public function mentor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $file = $request->file('file');
        $filename = $file->hashName();
        $file->move(public_path('file-uploads'), $filename);
        try {
            Excel::import(new MentorImport(), public_path('file-uploads') . '/' . $filename);
            File::delete(public_path('file-uploads') . '/' . $filename);
            return $this->notifyResponse('success', 'Upload succeeded', 'Your data has been successfully uploaded.');
        } catch (\Throwable $th) {
            File::delete(public_path('file-uploads') . '/' . $filename);
            return $this->notifyResponse('failed', 'Upload failed', $th->getMessage());
        }
    }

    public function mentee(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $file = $request->file('file');
        $filename = $file->hashName();
        $file->move(public_path('file-uploads'), $filename);
        try {
            Excel::import(new MenteesImport(), public_path('file-uploads') . '/' . $filename);
            File::delete(public_path('file-uploads') . '/' . $filename);
            return $this->notifyResponse('success', 'Upload succeeded', 'Your data has been successfully uploaded.');
        } catch (\Throwable $th) {
            File::delete(public_path('file-uploads') . '/' . $filename);
            return $this->notifyResponse('failed', 'Upload failed', $th->getMessage());
        }
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }
}
