<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home()
    {
        $role = Auth::user()->role;
        if ($role == 'mentor') {
            return redirect()->intended('mentor/dashboard');
        } elseif ($role == 'kmf') {
            return redirect()->intended('kmf/dashboard');
        } else {
            return redirect()->intended('kmp/dashboard');
        }
    }

    public function index()
    {
        return view('mentor.home');
    }

    public function kmfIndex()
    {
        return view('kmf.home');
    }

    public function kmpIndex()
    {
        return view('kmp.home');
    }
}
