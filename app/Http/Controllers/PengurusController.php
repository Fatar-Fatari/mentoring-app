<?php

namespace App\Http\Controllers;

use App\Models\Fakultas;
use App\Models\TahunAjar;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PengurusController extends Controller
{
    public function index(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = User::where('role', 'kmf')->get();
            return view('kmp.pengurus.table_data', compact('data'));
        }
        $fakultas = Fakultas::orderBy('nama')->get();
        return view('kmp.pengurus.index', compact('fakultas'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fakultas_id' => 'required',
            'nama' => 'required',
            'username' => [
                'required',
                'alpha_dash:ascii',
                Rule::unique('users', 'username')->where(function ($query) {
                    return $query->where('role', 'kmf');
                }),
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->where(function ($query) {
                    return $query->where('role', 'kmf');
                }),
            ],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Add data to database and send response
        User::create([
            'fakultas_id' => $request->fakultas_id,
            'nama' => ucwords($request->nama),
            'username' => strtolower($request->username),
            'email' => $request->email,
            'password' => bcrypt('password'),
            'role' => 'kmf',
        ]);
        return $this->notifyResponse('success', 'Submit succeeded', 'Your data has been successfully submited.');
    }

    public function show(User $pengurus)
    {
        return response()->json($pengurus);
    }

    public function update(Request $request, User $pengurus)
    {
        $validator = Validator::make($request->all(), [
            'fakultas_id' => 'required',
            'nama' => 'required',
            'username' => [
                'required',
                'alpha_dash:ascii',
                Rule::unique('users', 'username')->ignore($pengurus->id)->where(function ($query) {
                    return $query->where('role', 'kmf');
                }),
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($pengurus->id)->where(function ($query) {
                    return $query->where('role', 'kmf');
                }),
            ],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Update data to database and send response
        $pengurus->update([
            'fakultas_id' => $request->fakultas_id,
            'nama' => ucwords($request->nama),
            'username' => strtolower($request->username),
            'email' => $request->email,
        ]);
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    public function destroy(User $pengurus)
    {
        $pengurus->delete();
        return $this->notifyResponse('success', 'Delete succeeded', 'Your data has been successfully deleted.');
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }
}
