<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\BuktiKegiatan;
use App\Models\Kelas;
use App\Models\Mentee;
use App\Models\TahunAjar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AbsensiController extends Controller
{
    public function index(Request $request)
    {
        $tahun_ajar = TahunAjar::where('active', true)->first();
        $mentor = Auth::user();
        $kelas = Kelas::where('tahun_ajar_id', $tahun_ajar->id)
            ->where('mentor_id', $mentor->id)->first();

        if ($request->option == 'load_table_data') {
            $bukti = BuktiKegiatan::where('kelas_id', $kelas->id)
                ->where('pertemuan', $request->pertemuan)->first();
            $data = Mentee::select('mentees.*', 'absensis.id as absensi', 'absensis.kehadiran')
                ->leftJoin('absensis', 'absensis.mentee_id', '=', 'mentees.id')
                ->where('mentees.kelas_id', $kelas->id)
                ->where('absensis.kelas_id', $kelas->id)
                ->where('absensis.pertemuan', $request->pertemuan)
                ->get();
            if (!$bukti) {
                $this->_setup_bukti($kelas->id, $request->pertemuan);
                $bukti = BuktiKegiatan::where('kelas_id', $kelas->id)
                    ->where('pertemuan', $request->pertemuan)->first();
            }
            if ($data->isEmpty()) {
                $this->_setup_absensi($kelas->id, $request->pertemuan);
                $data = Mentee::select('mentees.*', 'absensis.id as absensi', 'absensis.kehadiran')
                    ->leftJoin('absensis', 'absensis.mentee_id', '=', 'mentees.id')
                    ->where('mentees.kelas_id', $kelas->id)
                    ->where('absensis.kelas_id', $kelas->id)
                    ->where('absensis.pertemuan', $request->pertemuan)
                    ->get();
            }
            return view('mentor.absensi.table_data', compact('bukti', 'data'));
        }

        return view('mentor.absensi.index', compact('kelas'));
    }

    public function store(Request $request)
    {
        if ($request->bukti_id) {
            $bukti = BuktiKegiatan::find($request->bukti_id);

            if (File::exists(public_path('file-uploads/bukti_kegiatan') . '/' . $bukti->file)) {
                File::delete(public_path('file-uploads/bukti_kegiatan') . '/' . $bukti->file);
            }
            $file = $request->file('file');
            $filename = $file->hashName();
            $file->move(public_path('file-uploads/bukti_kegiatan'), $filename);

            $bukti->update(['file' => $filename]);
            return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
        }

        $alldata = [];
        foreach ($request->kehadiran as $item) {
            $data = explode('-', $item);
            array_push($alldata, $data);
        }

        foreach ($alldata as $item) {
            Absensi::find($item[0])->update(['kehadiran' => $item[1]]);
        }
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }

    private function _setup_absensi($kelas_id, $pertemuan)
    {
        for ($i = 1; $i <= 14; $i++) {
            $data = Mentee::where('mentees.kelas_id', $kelas_id)
                ->get();
            foreach ($data as $item) {
                Absensi::create([
                    'kelas_id' => $kelas_id,
                    'mentee_id' => $item->id,
                    'pertemuan' => $i,
                    'kehadiran' => false,
                ]);
            }
        }
    }

    private function _setup_bukti($kelas_id, $pertemuan)
    {
        for ($i = 1; $i <= 14; $i++) {
            BuktiKegiatan::create([
                'kelas_id' => $kelas_id,
                'pertemuan' => $i,
            ]);
        }
    }
}
