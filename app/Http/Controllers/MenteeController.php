<?php

namespace App\Http\Controllers;

use App\Models\Fakultas;
use App\Models\Mentee;
use App\Models\TahunAjar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MenteeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = Mentee::all();
            return view('kmp.mentee.table_data', compact('data'));
        }
        $tahun_ajar = TahunAjar::where('active', true)->first();
        $fakultas = Fakultas::orderBy('nama')->get();
        return view('kmp.mentee.index', compact('tahun_ajar', 'fakultas'));
    }

    public function kmfIndex(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = Mentee::where('fakultas_id', Auth::user()->fakultas_id)->orderBy('nama')->get();
            return view('kmf.mentee.table_data', compact('data'));
        }
        return view('kmf.mentee.index');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fakultas_id' => 'required',
            'nama' => 'required',
            'nim' => 'required|unique:mentees,nim',
            'gender' => 'required',
            'no_hp' => 'required|unique:mentees,no_hp',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Add data to database and send response
        Mentee::create([
            'tahun_ajar_id' => $request->tahun_ajar_id,
            'fakultas_id' => $request->fakultas_id,
            'nama' => ucwords($request->nama),
            'nim' => strtoupper($request->nim),
            'gender' => $request->gender,
            'no_hp' => $request->no_hp,
        ]);
        return $this->notifyResponse('success', 'Submit succeeded', 'Your data has been successfully submited.');
    }

    public function show(Mentee $mentee)
    {
        return response()->json($mentee);
    }

    public function update(Request $request, Mentee $mentee)
    {
        $validator = Validator::make($request->all(), [
            'fakultas_id' => 'required',
            'nama' => 'required',
            'nim' => 'required|unique:mentees,nim,' . $mentee->id,
            'gender' => 'required',
            'no_hp' => 'required|unique:mentees,no_hp,' . $mentee->id,
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Update data to database and send response
        $mentee->update([
            'fakultas_id' => $request->fakultas_id,
            'nama' => ucwords($request->nama),
            'nim' => strtoupper($request->nim),
            'gender' => $request->gender,
            'no_hp' => $request->no_hp,
        ]);
        return $this->notifyResponse('success', 'Update succeeded', 'Your data has been successfully updated.');
    }

    public function destroy(Mentee $mentee)
    {
        $mentee->delete();
        return $this->notifyResponse('success', 'Delete succeeded', 'Your data has been successfully deleted.');
    }

    private function notifyResponse($status, $title, $message)
    {
        return view('layouts.partials.notify', compact('status', 'title', 'message'));
    }
}
