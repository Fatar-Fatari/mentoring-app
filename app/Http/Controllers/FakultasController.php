<?php

namespace App\Http\Controllers;

use App\Models\Fakultas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FakultasController extends Controller
{
    public function index(Request $request)
    {
        if ($request->option == 'load_table_data') {
            $data = Fakultas::all();
            return view('kmp.fakultas.table_data', compact('data'));
        }
        return view('kmp.fakultas.index');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Add data to database and send response
        try {
            Fakultas::create([
                'kode' => $request->kode,
                'nama' => $request->nama,
            ]);
            return view('layouts.partials.notify', [
                'status' => 'success',
                'title' => 'Submit succeeded',
                'message' => 'Your data has been successfully submited.',
            ]);
        } catch (\Throwable $th) {
            return view('layouts.partials.notify', [
                'status' => 'failed',
                'title' => 'Submit failed',
                'message' => 'Something wrong. The submit cannot be processed.',
            ]);
        }
    }

    public function show(Request $request, Fakultas $fakultas)
    {
        if ($request->has('kelas')) {
            return response()->json($fakultas->kelas);
        }
        return response()->json($fakultas);
    }

    public function update(Request $request, Fakultas $fakultas)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Update data to database and send response
        try {
            $fakultas->update([
                'kode' => $request->kode,
                'nama' => $request->nama,
            ]);
            return view('layouts.partials.notify', [
                'status' => 'success',
                'title' => 'Update succeeded',
                'message' => 'Your data has been successfully updated.',
            ]);
        } catch (\Throwable $th) {
            return view('layouts.partials.notify', [
                'status' => 'failed',
                'title' => 'Update failed',
                'message' => 'Something wrong. The update cannot be processed.',
            ]);
        }
    }

    public function destroy(Fakultas $fakultas)
    {
        try {
            $fakultas->delete();
            return view('layouts.partials.notify', [
                'status' => 'success',
                'title' => 'Delete succeeded',
                'message' => 'Your data has been successfully deleted.',
            ]);
        } catch (\Throwable $th) {
            return view('layouts.partials.notify', [
                'status' => 'failed',
                'title' => 'Delete failed',
                'message' => 'Something wrong. The delete cannot be processed.',
            ]);
        }
    }
}
