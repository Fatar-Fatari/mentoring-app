<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    use HasFactory;

    protected $fillable = ['tahun_ajar_id', 'nama', 'file'];

    public function tahun_ajar()
    {
        return $this->belongsTo(TahunAjar::class);
    }
}
