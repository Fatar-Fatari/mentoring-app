<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    use HasFactory;

    protected $fillable = ['kode', 'nama'];

    public function kelas()
    {
        return $this->hasMany(Kelas::class);
    }

    public function mentee()
    {
        return $this->hasMany(Mentee::class);
    }

    public function mentor()
    {
        return $this->hasMany(User::class);
    }
}
