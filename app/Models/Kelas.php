<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;

    protected $fillable = ['tahun_ajar_id', 'fakultas_id', 'mentor_id', 'nama', 'tipe'];

    public function tahun_ajar()
    {
        return $this->belongsTo(TahunAjar::class);
    }

    public function fakultas()
    {
        return $this->belongsTo(Fakultas::class, 'fakultas_id');
    }

    public function mentor()
    {
        return $this->belongsTo(User::class, 'mentor_id');
    }

    public function mentee()
    {
        return $this->hasMany(Mentee::class);
    }

    public function absensi()
    {
        return $this->hasMany(Absensi::class);
    }

    public function bukti_kegiatan()
    {
        return $this->hasMany(BuktiKegiatan::class);
    }

    public function nilai()
    {
        return $this->hasMany(Nilai::class);
    }
}
