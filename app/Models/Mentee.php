<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mentee extends Model
{
    use HasFactory;

    protected $fillable = ['tahun_ajar_id', 'fakultas_id', 'nama', 'nim', 'gender', 'no_hp', 'kelas_id'];

    protected $with = ['absensi', 'nilai'];

    public function tahun_ajar()
    {
        return $this->belongsTo(TahunAjar::class);
    }

    public function fakultas()
    {
        return $this->belongsTo(Fakultas::class, 'fakultas_id');
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function absensi()
    {
        return $this->hasMany(Absensi::class);
    }

    public function nilai()
    {
        return $this->hasMany(Nilai::class);
    }
}
