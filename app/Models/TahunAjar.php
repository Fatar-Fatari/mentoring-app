<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TahunAjar extends Model
{
    use HasFactory;

    protected $fillable = ['tahun_ajar', 'start', 'end', 'active'];

    public function kelas()
    {
        return $this->hasMany(Kelas::class);
    }

    public function mentee()
    {
        return $this->hasMany(Mentee::class);
    }

    public function materi()
    {
        return $this->hasMany(Materi::class);
    }
}
