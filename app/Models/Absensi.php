<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    use HasFactory;

    protected $fillable = ['kelas_id', 'mentee_id', 'pertemuan', 'kehadiran'];

    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function mentee()
    {
        return $this->belongsTo(Mentee::class);
    }
}
