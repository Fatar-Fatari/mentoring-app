<?php

namespace App\Imports;

use App\Models\Mentee;
use App\Models\TahunAjar;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class MenteesImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $tahun_ajar = TahunAjar::where('active', true)->first();
        return new Mentee([
            'nama' => $row['nama'],
            'nim' => $row['nim'],
            'gender' => $row['gender'],
            'no_hp' => $row['no_hp'],
            'tahun_ajar_id' => $tahun_ajar->id,
            'fakultas_id' => $row['fakultas'] ?? 1,
        ]);
    }

    public function rules(): array
    {
        return [
            'nama' => 'required',
            'nim' => 'required|unique:mentees,nim',
            'gender' => 'required',
            'no_hp' => 'required|unique:mentees,no_hp',
        ];
    }
}
