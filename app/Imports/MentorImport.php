<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class MentorImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new User([
            'nama' => $row['nama'],
            'username' => $row['username'],
            'email' => $row['email'],
            'password' => bcrypt('password'),
            'role' => 'mentor',
            'fakultas_id' => $row['fakultas'] ?? 1,
        ]);
    }

    public function rules(): array
    {
        return [
            'nama' => 'required',
            'username' => 'required|alpha_dash:ascii|unique:users,username',
            'email' => 'required|email|unique:users,email',
        ];
    }
}
