<?php

use App\Http\Controllers\AbsensiController;
use App\Http\Controllers\FakultasController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\MateriController;
use App\Http\Controllers\MenteeController;
use App\Http\Controllers\MentorController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\PengurusController;
use App\Http\Controllers\TahunAjarController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', [HomeController::class, 'home'])->middleware('auth')->name('home');

Auth::routes();

// Mentor routes
Route::middleware(['auth', 'user-access:mentor'])->prefix('mentor')->name('mentor.')->group(function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
    Route::get('/materi', [MateriController::class, 'mentorIndex'])->name('materi.mentorIndex');
    Route::resource('/absensi', AbsensiController::class);
    Route::resource('/nilai', NilaiController::class);
    Route::get('/laporan', [LaporanController::class, 'laporanMentor'])->name('laporan.index');
});

// KMF routes
Route::middleware(['auth', 'user-access:kmf'])->prefix('kmf')->name('kmf.')->group(function () {
    Route::get('/dashboard', [HomeController::class, 'kmfIndex'])->name('dashboard');
    Route::get('/mentor', [MentorController::class, 'kmfIndex'])->name('mentor.index');
    Route::get('/mentee', [MenteeController::class, 'kmfIndex'])->name('mentee.index');
    Route::resource('/kelas', KelasController::class)->except(['create'])
        ->parameters(['kelas' => 'kelas']);
    Route::post('/add_mentee_to_kelas', [KelasController::class, 'add_mentee_to_kelas'])->name('add_mentee_to_kelas');
    Route::post('/remove_mentee_from_kelas', [KelasController::class, 'remove_mentee_from_kelas'])->name('remove_mentee_from_kelas');
    Route::get('/laporan', [LaporanController::class, 'laporanFakultas'])->name('laporan.index');
    Route::post('/laporan_filter', [LaporanController::class, 'laporanFilterFakultas'])->name('laporan.filter');
});

// KMP routesLove
Route::middleware(['auth', 'user-access:kmp'])->prefix('kmp')->name('kmp.')->group(function () {
    Route::get('/dashboard', [HomeController::class, 'kmpIndex'])->name('dashboard');
    Route::resource('/tahun_ajar', TahunAjarController::class)->except(['create', 'edit']);
    Route::post('/tahun_ajar_update_active', [TahunAjarController::class, 'update_active'])
        ->name('tahun_ajar_update_active');
    Route::resource('/fakultas', FakultasController::class)->except(['create', 'edit'])
        ->parameters(['fakultas' => 'fakultas']);
    Route::resource('/kelas', KelasController::class)->except(['create', 'edit'])
        ->parameters(['kelas' => 'kelas']);
    Route::resource('/pengurus', PengurusController::class)->except(['create', 'edit'])
        ->parameters(['pengurus' => 'pengurus']);
    Route::resource('/mentor', MentorController::class)->except(['create', 'edit']);
    Route::resource('/mentee', MenteeController::class)->except(['create', 'edit']);
    Route::resource('/materi', MateriController::class)->except(['create', 'edit']);

    // Import route
    Route::post('/import/pengurus', [ImportController::class, 'pengurus'])->name('import.pengurus');
    Route::post('/import/mentor', [ImportController::class, 'mentor'])->name('import.mentor');
    Route::post('/import/mentee', [ImportController::class, 'mentee'])->name('import.mentee');
});

Route::get('seeding-test', function () {
    $faker = Faker\Factory::create('id_ID');

    for ($i = 1; $i <= 50; $i++) {

        // insert data ke table pegawai menggunakan Faker
        DB::table('mentees')->insert([
            'nama' => $faker->name,
            'nim' => $faker->randomElement(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']) . $faker->randomNumber(9, true),
            'gender' => $faker->randomElement(['L', 'P']),
            'no_hp' => $faker->phoneNumber,
            'tahun_ajar_id' => 1,
            'fakultas_id' => $faker->numberBetween(1, 12)
        ]);
        DB::table('users')->insert([
            'nama' => $faker->name(),
            'username' => $faker->userName(),
            'email' => $faker->email(),
            'password' => bcrypt('password'),
            'fakultas_id' => $faker->numberBetween(1, 3),
            'role' => 'mentor'
        ]);
    }
});
