<script>
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    function create_datatable(element) {
        // Initialize datatable
        var dataTable = element.DataTable({
            destroy: true,
            columnDefs: [{
                searchable: false,
                orderable: false,
                targets: [0, -1],
            }, ],
            order: [
                [1, "asc"]
            ],
            language: {
                emptyTable: `<div class="empty">
                                    <div class="empty-img"><img src="{{ asset('static/illustrations/undraw_quitting_time_dm8t.svg') }}" height="128"
                                            alt="">
                                    </div>
                                    <p class="empty-title">No data available in table</p>
                                    <p class="empty-subtitle text-muted">
                                        Table is empty, please add the data.
                                    </p>
                                </div>`,
                zeroRecords: `<div class="empty">
                                    <div class="empty-img"><img src="{{ asset('static/illustrations/undraw_quitting_time_dm8t.svg') }}" height="128"
                                            alt="">
                                    </div>
                                    <p class="empty-title">No matching records found</p>
                                    <p class="empty-subtitle text-muted">
                                        Try adjusting your search to find what you're looking for.
                                    </p>
                                </div>`,
                paginate: {
                    next: '<div class="d-flex align-items-center">Next<i class="ti ti-chevron-right"></i></div>',
                    previous: '<div class="d-flex align-items-center"><i class="ti ti-chevron-left"></i>Prev</div>',
                },
            },
        });

        // Setup datatable component layout
        $(".datatable-filter").empty().html($(".dataTables_filter"));
        $(".datatable-length").empty().html($(".dataTables_length"));
        $(".datatable-info").empty().html($(".dataTables_info"));
        $(".datatable-paginate").empty().html($(".dataTables_paginate"));

        // additional option
        dataTable
            .on("order.dt search.dt", function() {
                let i = 1;
                dataTable
                    .cells(null, 0, {
                        search: "applied",
                        order: "applied",
                    })
                    .every(function(cell) {
                        this.data(i++);
                    });
            })
            .draw();
    }

    function handleFormSubmitUpdate(
        form,
        url,
        successCallback,
        errorCallback,
        method = "post"
    ) {
        form.submit(function(event) {
            event.preventDefault();
            let id = form.find('input[name="id"]').val();
            var formData = new FormData(this);

            $.ajax({
                type: method,
                url: id ? `${url}/${id}` : url,
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                success: successCallback,
                error: errorCallback,
            });
        });
    }

    function handleFormDelete(form, url, successCallback, errorCallback) {
        handleFormSubmitUpdate(form, url, successCallback, errorCallback, "delete");
    }

    function handleFormErrors(error, inputSelectors) {
        $.each(error.responseJSON, function(key, value) {
            inputSelectors.forEach(function(inputSelector) {
                // Add error classes to input elements
                $(`${inputSelector}[name="${key}"]`).addClass("is-invalid");
            });

            // Display the error message
            $(`.error-${key}`)
                .removeClass("d-none")
                .addClass("d-block")
                .html(value);
        });
    }

    function clearForm(element) {
        element.trigger("reset");
        element.find(".invalid-feedback").each(function() {
            $(this).removeClass("d-block").addClass("d-none");
        });
        element.find("input").each(function() {
            $(this).removeClass("is-invalid");
        });
        element.find("select").each(function() {
            $(this).removeClass("is-invalid");
        });
        element.find("textarea").each(function() {
            $(this).removeClass("is-invalid");
        });
    }

    function notify(element) {
        if ($("#modal-notify").length) {
            $("#modal-notify").remove();
        }
        $("body").append(element);
        $("#modal-notify").modal("show");
    }

    function loadDataTable(url) {
        $.ajax({
            type: "get",
            url: url,
            success: function(response) {
                // replacing
                $(".content-data").html(response);
                // reinitialize
                create_datatable($(".datatable"));
            },
        });
    }

    $(function() {
        $(".litepicker").each(function(index) {
            new Litepicker({
                element: $(this)[0],
                buttonText: {
                    previousMonth: `<i class="ti ti-chevron-left"></i>`,
                    nextMonth: `<i class="ti ti-chevron-right"></i>`,
                },
            });
        });
    });
</script>
