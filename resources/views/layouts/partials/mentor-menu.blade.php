<ul class="navbar-nav pt-lg-3">
    <li class="nav-item {{ request()->is('mentor/dashboard') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('mentor.dashboard') }}">
            <span
                class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/home -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                    stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                    stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M5 12l-2 0l9 -9l9 9l-2 0"></path>
                    <path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7"></path>
                    <path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6"></path>
                </svg>
            </span>
            <span class="nav-link-title">
                Home
            </span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('mentor/materi') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('mentor.materi.mentorIndex') }}">
            <span
                class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/checkbox -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-book" width="24"
                    height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M3 19a9 9 0 0 1 9 0a9 9 0 0 1 9 0"></path>
                    <path d="M3 6a9 9 0 0 1 9 0a9 9 0 0 1 9 0"></path>
                    <path d="M3 6l0 13"></path>
                    <path d="M12 6l0 13"></path>
                    <path d="M21 6l0 13"></path>
                </svg>
            </span>
            <span class="nav-link-title">
                Materi Kelas
            </span>
        </a>
    </li>
    <li
        class="nav-item {{ request()->is('mentor/absensi') || request()->is('mentor/nilai') ? 'active' : '' }} dropdown">
        <a class="nav-link dropdown-toggle" href="#navbar-layout" data-bs-toggle="dropdown" data-bs-auto-close="false"
            role="button" aria-expanded="true">
            <span
                class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/layout-2 -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                    stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                    stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M4 4m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v1a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                    <path d="M4 13m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v3a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                    <path d="M14 4m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v3a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                    <path d="M14 15m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v1a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                </svg>
            </span>
            <span class="nav-link-title">
                Kelas
            </span>
        </a>
        <div class="dropdown-menu {{ request()->is('mentor/absensi') || request()->is('mentor/nilai') ? 'show' : '' }}">
            <div class="dropdown-menu-columns">
                <div class="dropdown-menu-column">
                    <a class="dropdown-item {{ request()->is('mentor/absensi') ? 'active' : '' }}"
                        href="{{ route('mentor.absensi.index') }}">
                        Absensi
                    </a>
                    <a class="dropdown-item {{ request()->is('mentor/nilai') ? 'active' : '' }}"
                        href="{{ route('mentor.nilai.index') }}">
                        Nilai
                    </a>
                </div>
            </div>
        </div>
    </li>
    <li class="nav-item {{ request()->is('mentor/laporan') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('mentor.laporan.index') }}">
            <span
                class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/checkbox -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-book" width="24"
                    height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M3 19a9 9 0 0 1 9 0a9 9 0 0 1 9 0"></path>
                    <path d="M3 6a9 9 0 0 1 9 0a9 9 0 0 1 9 0"></path>
                    <path d="M3 6l0 13"></path>
                    <path d="M12 6l0 13"></path>
                    <path d="M21 6l0 13"></path>
                </svg>
            </span>
            <span class="nav-link-title">
                Laporan
            </span>
        </a>
    </li>
</ul>
