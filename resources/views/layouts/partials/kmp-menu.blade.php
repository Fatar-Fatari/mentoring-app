<ul class="navbar-nav pt-lg-3">
    <li class="nav-item {{ request()->is('kmp/dashboard') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
            <span
                class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/home -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                    stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                    stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M5 12l-2 0l9 -9l9 9l-2 0"></path>
                    <path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7"></path>
                    <path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6"></path>
                </svg>
            </span>
            <span class="nav-link-title">
                Home
            </span>
        </a>
    </li>
    <li
        class="nav-item {{ request()->is('kmp/tahun_ajar') || request()->is('kmp/fakultas') ? 'active' : '' }} dropdown">
        <a class="nav-link dropdown-toggle" href="#navbar-layout" data-bs-toggle="dropdown" data-bs-auto-close="false"
            role="button" aria-expanded="true">
            <span
                class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/layout-2 -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                    stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                    stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M4 4m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v1a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                    <path d="M4 13m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v3a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                    <path d="M14 4m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v3a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                    <path d="M14 15m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v1a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z">
                    </path>
                </svg>
            </span>
            <span class="nav-link-title">
                Master Data
            </span>
        </a>
        <div class="dropdown-menu {{ request()->is('kmp/tahun_ajar') || request()->is('kmp/fakultas') ? 'show' : '' }}">
            <div class="dropdown-menu-columns">
                <div class="dropdown-menu-column">
                    <a class="dropdown-item {{ request()->is('kmp/tahun_ajar') ? 'active' : '' }}"
                        href="{{ route('kmp.tahun_ajar.index') }}">
                        Tahun ajar
                    </a>
                    <a class="dropdown-item {{ request()->is('kmp/fakultas') ? 'active' : '' }}"
                        href="{{ route('kmp.fakultas.index') }}">
                        Fakultas
                    </a>
                </div>
            </div>
        </div>
    </li>
    <li
        class="nav-item {{ request()->is('kmp/pengurus') || request()->is('kmp/mentee') || request()->is('kmp/mentor') ? 'active' : '' }} dropdown">
        <a class="nav-link dropdown-toggle" href="#navbar-layout" data-bs-toggle="dropdown" data-bs-auto-close="false"
            role="button" aria-expanded="true">
            <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24"
                    height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M9 7m-4 0a4 4 0 1 0 8 0a4 4 0 1 0 -8 0"></path>
                    <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
                    <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                    <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
                </svg>
            </span>
            <span class="nav-link-title">
                Manage SDM
            </span>
        </a>
        <div
            class="dropdown-menu {{ request()->is('kmp/mentor') || request()->is('kmp/mentee') || request()->is('kmp/pengurus') ? 'show' : '' }}">
            <div class="dropdown-menu-columns">
                <div class="dropdown-menu-column">
                    <a class="dropdown-item {{ request()->is('kmp/pengurus') ? 'active' : '' }}"
                        href="{{ route('kmp.pengurus.index') }}">
                        Pengurus
                    </a>
                    <a class="dropdown-item {{ request()->is('kmp/mentor') ? 'active' : '' }}"
                        href="{{ route('kmp.mentor.index') }}">
                        Mentor
                    </a>
                    <a class="dropdown-item {{ request()->is('kmp/mentee') ? 'active' : '' }}"
                        href="{{ route('kmp.mentee.index') }}">
                        Mentee
                    </a>
                    {{-- <a class="dropdown-item {{ request()->is('kmp/kelas') ? 'active' : '' }}"
                        href="{{ route('kmp.kelas.index') }}">
                        Kelas
                    </a> --}}
                </div>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('kmp.materi.index') }}">
            <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-book" width="24"
                    height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M3 19a9 9 0 0 1 9 0a9 9 0 0 1 9 0"></path>
                    <path d="M3 6a9 9 0 0 1 9 0a9 9 0 0 1 9 0"></path>
                    <path d="M3 6l0 13"></path>
                    <path d="M12 6l0 13"></path>
                    <path d="M21 6l0 13"></path>
                </svg>
            </span>
            <span class="nav-link-title">
                Materi kelas
            </span>
        </a>
    </li>
</ul>
