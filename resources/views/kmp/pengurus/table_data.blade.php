<div class="card-body border-bottom py-3">
    <div class="d-flex align-items-center justify-content-between">
        <div class="text-muted datatable-length"></div>
        <div class="ms-auto text-muted datatable-filter"></div>
    </div>
</div>
<div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap datatable">
        <thead>
            <tr>
                <th class="w-1">No.</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Email</th>
                <th>Fakultas</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @php
                $color = ['azure', 'indigo', 'purple', 'red', 'orange', 'yellow', 'lime ', 'cyan', 'green', 'pink', 'teal', 'blue'];
            @endphp
            @foreach ($data as $index => $item)
                <tr>
                    <td></td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->username }}</td>
                    <td>{{ $item->email }}</td>
                    <td>
                        <span class="badge bg-{{ $color[$item->fakultas->id - 1] }}">
                            {{ $item->fakultas->kode }}
                        </span>
                    </td>
                    <td class="text-end">
                        <button type="button" class="btn btn-edit-data" data-id="{{ $item->id }}">
                            <i class="ti ti-edit me-2"></i>
                            Edit
                        </button>
                        <button type="button" class="btn btn-delete-data" data-id="{{ $item->id }}">
                            <i class="ti ti-trash me-2"></i>
                            Delete
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="card-footer d-flex align-items-center justify-content-between">
    <div class="datatable-info"></div>
    <div class="datatable-paginate"></div>
</div>
