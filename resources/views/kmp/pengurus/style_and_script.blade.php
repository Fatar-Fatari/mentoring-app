@push('style')
    <style>
        .dataTables_length label,
        .dataTables_filter label {
            display: flex;
            gap: 8px;
        }

        .dataTables_paginate ul {
            margin: auto 0;
        }
    </style>
@endpush

@push('script')
    <script>
        // Load table data from ajax request
        loadDataTable("{{ url('kmp/pengurus?option=load_table_data') }}");

        $(document).ready(function() {
            // On form submit
            handleFormSubmitUpdate(
                $('#form-submit'), "{{ route('kmp.pengurus.store') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/pengurus?option=load_table_data') }}");
                    clearForm($('#form-submit'));
                    $('#modal-add').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On form update
            handleFormSubmitUpdate(
                $('#form-update'), "{{ url('kmp/pengurus') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/pengurus?option=load_table_data') }}");
                    clearForm($('#form-update'));
                    $('#modal-update').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On delete
            handleFormDelete(
                $('#form-delete'), "{{ url('kmp/pengurus') }}",
                function(response) {
                    loadDataTable("{{ url('kmp/pengurus?option=load_table_data') }}");
                    $('#modal-delete').modal('hide');
                    notify(response);
                });

            // On form import
            handleFormSubmitUpdate(
                $('#form-import'), "{{ route('kmp.import.pengurus') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/pengurus?option=load_table_data') }}");
                    clearForm($('#form-import'));
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On add button click
            $(document).on('click', '.btn-add-data', function(e) {
                clearForm($('#form-submit'));
            });

            // On edit button click
            $(document).on('click', '.btn-edit-data', function(e) {
                clearForm($('#form-update'));
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ url('kmp/pengurus') }}" + `/${id}`,
                    type: "GET",
                    cache: false,
                    success: function(response) {
                        //fill data to form
                        $('#form-update').find('input[name="id"]').val(response.id);
                        $('#form-update').find('select[name="fakultas_id"]').val(response
                            .fakultas_id).change();
                        $('#form-update').find('input[name="nama"]').val(response.nama);
                        $('#form-update').find('input[name="username"]').val(response.username);
                        $('#form-update').find('input[name="email"]').val(response.email);

                        //open modal
                        $('#modal-update').modal('show');
                    }
                });
            });

            // On delete button click
            $(document).on('click', '.btn-delete-data', function(e) {
                let id = $(this).data('id');
                $('#modal-delete').modal('show');
                $('#modal-delete').find('input[name="id"]').val(id);
            });
        });
    </script>
@endpush
