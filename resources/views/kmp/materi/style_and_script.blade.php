@push('style')
    <style>
        .dataTables_length label,
        .dataTables_filter label {
            display: flex;
            gap: 8px;
        }

        .dataTables_paginate ul {
            margin: auto 0;
        }

        .select2-results__option {
            font-size: 30px;
        }
    </style>
@endpush

@push('script')
    <script>
        // Load table data from ajax request
        loadDataTable("{{ url('kmp/materi?option=load_table_data') }}");

        $(document).ready(function() {
            // On form submit
            handleFormSubmitUpdate(
                $('#form-submit'), "{{ route('kmp.materi.store') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/materi?option=load_table_data') }}");
                    clearForm($('#form-submit'));
                    $('#modal-add').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On form update
            handleFormSubmitUpdate(
                $('#form-update'), "{{ url('kmp/materi') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/materi?option=load_table_data') }}");
                    clearForm($('#form-update'));
                    $('#modal-update').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On form update
            handleFormDelete(
                $('#form-delete'), "{{ url('kmp/materi') }}",
                function(response) {
                    loadDataTable("{{ url('kmp/materi?option=load_table_data') }}");
                    $('#modal-delete').modal('hide');
                    notify(response);
                });

            // On add button click
            $(document).on('click', '.btn-add-data', function(e) {
                clearForm($('#form-submit'));
            });

            // On edit button click
            $(document).on('click', '.btn-edit-data', function(e) {
                clearForm($('#form-update'));
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ url('kmp/materi') }}" + `/${id}`,
                    type: "GET",
                    cache: false,
                    success: function(response) {
                        //fill data to form
                        $('#form-update').find('input[name="id"]').val(response.id);
                        $('#form-update').find('input[name="nama"]').val(response.nama);

                        //open modal
                        $('#modal-update').modal('show');
                    }
                });
            });

            // On delete button click
            $(document).on('click', '.btn-delete-data', function(e) {
                let id = $(this).data('id');
                $('#modal-delete').modal('show');
                $('#modal-delete').find('input[name="id"]').val(id);
            });
        });
    </script>
@endpush
