@push('style')
    <style>
        .dataTables_length label,
        .dataTables_filter label {
            display: flex;
            gap: 8px;
        }

        .dataTables_paginate ul {
            margin: auto 0;
        }

        .select2-results__option {
            font-size: 30px;
        }
    </style>
@endpush

@push('script')
    <script>
        // Load table data from ajax request
        loadDataTable("{{ url('kmp/kelas?option=load_table_data') }}");

        $(document).ready(function() {
            // On form submit
            handleFormSubmitUpdate(
                $('#form-submit'), "{{ route('kmp.kelas.store') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/kelas?option=load_table_data') }}");
                    clearForm($('#form-submit'));
                    $('#modal-add').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On form update
            handleFormSubmitUpdate(
                $('#form-update'), "{{ url('kmp/kelas') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/kelas?option=load_table_data') }}");
                    clearForm($('#form-update'));
                    $('#modal-update').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On form update
            handleFormDelete(
                $('#form-delete'), "{{ url('kmp/kelas') }}",
                function(response) {
                    loadDataTable("{{ url('kmp/kelas?option=load_table_data') }}");
                    $('#modal-delete').modal('hide');
                    notify(response);
                });

            // On add button click
            $(document).on('click', '.btn-add-data', function(e) {
                clearForm($('#form-submit'));
            });

            // On edit button click
            $(document).on('click', '.btn-edit-data', function(e) {
                clearForm($('#form-update'));
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ url('kmp/kelas') }}" + `/${id}`,
                    type: "GET",
                    cache: false,
                    success: function(response) {
                        //fill data to form
                        $('#form-update').find('input[name="id"]').val(response.id);
                        $('#form-update').find('select[name="fakultas_id"]').val(response
                            .fakultas_id).change();
                        $('#form-update').find('input[name="nama"]').val(response.nama);

                        //open modal
                        $('#modal-update').modal('show');
                    }
                });
            });

            // On delete button click
            $(document).on('click', '.btn-delete-data', function(e) {
                let id = $(this).data('id');
                $('#modal-delete').modal('show');
                $('#modal-delete').find('input[name="id"]').val(id);
            });

            // On edit button click
            load_kelas();
            $(document).on('change', '.manage-select-fakultas', function(e) {
                load_kelas();
            });

            $('.mentor-select').each(function() {
                $(this).select2({
                    theme: 'bootstrap-5',
                    dropdownParent: $(this).closest('.modal'),
                });
            });
        });

        function load_kelas() {
            let id = $('.manage-select-fakultas').val();
            $.ajax({
                url: "{{ url('kmp/fakultas') }}" + `/${id}?kelas`,
                type: "GET",
                cache: false,
                success: function(response) {
                    console.log(response);
                    //fill data to form
                    $('.manage-select-kelas').empty();
                    $.each(response, function(index, item) {
                        $('.manage-select-kelas').append(
                            `<option value="${item.id}">${item.nama}</option>`);
                    })
                }
            });
        }
    </script>
@endpush
