@extends('layouts.app')
@section('page-title')
    <div>
        Manage SDM <i class="ti ti-chevron-right"></i> Mentor
    </div>
@endsection

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs nav-fill" data-bs-toggle="tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a href="#tabs-home-3" class="nav-link active" data-bs-toggle="tab" aria-selected="true"
                            role="tab">
                            Data list</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a href="#tabs-profile-3" class="nav-link" data-bs-toggle="tab" aria-selected="false" tabindex="-1"
                            role="tab">
                            Upload batch</a>
                    </li>
                </ul>
            </div>
            <div class="card-body p-0">
                <div class="tab-content">
                    <div class="tab-pane active show" id="tabs-home-3" role="tabpanel">
                        <div class="card border-0">
                            <div class="card-header">
                                <div class="w-100 d-flex align-items-center justify-content-between">
                                    <h3 class="card-title">All mentor data</h3>
                                    <button type="button" class="btn btn-primary btn-add-data" data-bs-toggle="modal"
                                        data-bs-target="#modal-add">
                                        <i class="ti ti-plus me-2"></i>
                                        Add new data
                                    </button>
                                </div>
                            </div>
                            <div class="content-data">
                                <div class="row align-items-center m-4">
                                    <div class="col-2">
                                        <div class="avatar placeholder"></div>
                                    </div>
                                    <div class="col">
                                        <div class="placeholder placeholder-xs col-9"></div>
                                        <div class="placeholder placeholder-xs col-7"></div>
                                    </div>
                                    <div class="col">
                                        <div class="placeholder placeholder-xs col-9"></div>
                                        <div class="placeholder placeholder-xs col-7"></div>
                                    </div>
                                    <div class="col ms-auto text-end">
                                        <div class="placeholder placeholder-xs col-8"></div>
                                        <div class="placeholder placeholder-xs col-10"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabs-profile-3" role="tabpanel">
                        <div class="card border-0">
                            <div class="card-body">
                                <form id="form-import" action="javascript:void(0)" autocomplete="off" novalidate>
                                    @csrf
                                    @method('post')
                                    <div class="mb-3">
                                        <div class="form-label">File .xlxs</div>
                                        <input type="file" class="form-control" name="file">
                                        <div class="invalid-feedback d-none error-file">Invalid feedback</div>
                                    </div>
                                    <div class="alert alert-info" role="alert">
                                        <h4 class="alert-title">Looking for template ?</h4>
                                        <a href="{{ asset('file-templates/mentorImportTemplate.xlsx') }}" class="text-dark"
                                            download>Download here.</a>
                                    </div>

                                    <button type="submit" class="btn btn-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-upload"
                                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                            stroke="currentColor" fill="none" stroke-linecap="round"
                                            stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <path d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2 -2v-2"></path>
                                            <path d="M7 9l5 -5l5 5"></path>
                                            <path d="M12 4l0 12"></path>
                                        </svg>
                                        Upload
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-add" tabindex="-1" role="dialog" aria-hidden="true" data-focus="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="javascript:void(0)" method="post" id="form-submit">
                    @csrf
                    @method('post')
                    <div class="modal-header">
                        <h5 class="modal-title">New data</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label class="form-label">Fakultas</label>
                            <select class="form-select" name="fakultas_id">
                                @foreach ($fakultas as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback d-none error-fakultas_id">Invalid feedback</div>
                        </div>
                        <hr>
                        <div class="mb-3">
                            <label class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama">
                            <div class="invalid-feedback d-none error-nama">Invalid feedback</div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Username</label>
                                    <input type="text" class="form-control" name="username" placeholder="Username">
                                    <div class="invalid-feedback d-none error-username">Invalid feedback</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email"
                                        placeholder="your@mail.com">
                                    <div class="invalid-feedback d-none error-email">Invalid feedback</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-primary ms-auto">
                            <i class="ti ti-plus me-1"></i>
                            Add new data
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-update" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="javascript:void(0)" method="post" id="form-update">
                    @csrf
                    @method('put')
                    <div class="modal-header">
                        <h5 class="modal-title">Edit data</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id">
                        <div class="mb-3">
                            <label class="form-label">Fakultas</label>
                            <select class="form-select" name="fakultas_id">
                                @foreach ($fakultas as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback d-none error-fakultas_id">Invalid feedback</div>
                        </div>
                        <hr>
                        <div class="mb-3">
                            <label class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama kelas">
                            <div class="invalid-feedback d-none error-nama">Invalid feedback</div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Username</label>
                                    <input type="text" class="form-control" name="username" placeholder="Username">
                                    <div class="invalid-feedback d-none error-username">Invalid feedback</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email"
                                        placeholder="your@mail.com">
                                    <div class="invalid-feedback d-none error-email">Invalid feedback</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-primary ms-auto">
                            <i class="ti ti-refresh me-1"></i>
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-delete" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-status bg-danger"></div>
                <div class="modal-body text-center py-4">
                    <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-danger icon-lg" width="24"
                        height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                        stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M10.24 3.957l-8.422 14.06a1.989 1.989 0 0 0 1.7 2.983h16.845a1.989 1.989 0 0 0 1.7 -2.983l-8.423 -14.06a1.989 1.989 0 0 0 -3.4 0z" />
                        <path d="M12 9v4" />
                        <path d="M12 17h.01" />
                    </svg>
                    <h3>Are you sure?</h3>
                    <div class="text-muted">Do you really want to remove this data? What you've done cannot be undone.
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="w-100">
                        <div class="row">
                            <div class="col">
                                <button class="btn w-100" data-bs-dismiss="modal">
                                    Cancel
                                </button>
                            </div>
                            <div class="col">
                                <form action="javascript:void(0)"id="form-delete" method="post">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id">
                                    <button type="submit" class="btn btn-danger submit-text w-100">
                                        Delete
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('kmp.mentor.style_and_script')
