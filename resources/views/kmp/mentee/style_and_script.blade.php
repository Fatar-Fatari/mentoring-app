@push('style')
    <style>
        .dataTables_length label,
        .dataTables_filter label {
            display: flex;
            gap: 8px;
        }

        .dataTables_paginate ul {
            margin: auto 0;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        input[type="number"]::-webkit-outer-spin-button,
        input[type="number"]::-webkit-inner-spin-button {
            display: none;
        }
    </style>
@endpush

@push('script')
    <script>
        // Load table data from ajax request
        loadDataTable("{{ url('kmp/mentee?option=load_table_data') }}");

        $(document).ready(function() {
            // On form submit
            handleFormSubmitUpdate(
                $('#form-submit'), "{{ route('kmp.mentee.store') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/mentee?option=load_table_data') }}");
                    clearForm($('#form-submit'));
                    $('#modal-add').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On form update
            handleFormSubmitUpdate(
                $('#form-update'), "{{ url('kmp/mentee') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/mentee?option=load_table_data') }}");
                    clearForm($('#form-update'));
                    $('#modal-update').modal('hide');
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On delete
            handleFormDelete(
                $('#form-delete'), "{{ url('kmp/mentee') }}",
                function(response) {
                    loadDataTable("{{ url('kmp/mentee?option=load_table_data') }}");
                    $('#modal-delete').modal('hide');
                    notify(response);
                });

            // On form import
            handleFormSubmitUpdate(
                $('#form-import'), "{{ route('kmp.import.mentee') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ url('kmp/mentee?option=load_table_data') }}");
                    clearForm($('#form-import'));
                },
                function(error) {
                    console.log(error);
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On add button click
            $(document).on('click', '.btn-add-data', function(e) {
                clearForm($('#form-submit'));
            });

            // On edit button click
            $(document).on('click', '.btn-edit-data', function(e) {
                clearForm($('#form-update'));
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ url('kmp/mentee') }}" + `/${id}`,
                    type: "GET",
                    cache: false,
                    success: function(response) {
                        //fill data to form
                        $('#form-update').find('input[name="id"]').val(response.id);
                        $('#form-update').find('select[name="fakultas_id"]').val(response
                            .fakultas_id).change();
                        $('#form-update').find('input[name="nama"]').val(response.nama);
                        $('#form-update').find('input[name="nim"]').val(response.nim);
                        $('#form-update').find('select[name="gender"]').val(response
                            .gender).change();
                        $('#form-update').find('input[name="no_hp"]').val(response.no_hp);

                        //open modal
                        $('#modal-update').modal('show');
                    }
                });
            });

            // On delete button click
            $(document).on('click', '.btn-delete-data', function(e) {
                let id = $(this).data('id');
                $('#modal-delete').modal('show');
                $('#modal-delete').find('input[name="id"]').val(id);
            });
        });
    </script>
@endpush
