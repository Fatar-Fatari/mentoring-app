@push('style')
    <style>
        .dataTables_length label,
        .dataTables_filter label {
            display: flex;
            gap: 8px;
        }

        .dataTables_paginate ul {
            margin: auto 0;
        }
    </style>
@endpush

@push('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        // Load table data from ajax request
        load_table_data();

        $(document).ready(function() {
            // On form submit
            $('#form-submit').submit(function(event) {
                event.preventDefault();
                var form_element = $(this);
                var formdata = new FormData(this);

                $.ajax({
                    type: 'post',
                    url: "{{ route('kmp.fakultas.store') }}",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        load_table_data();
                        clear_form(form_element);
                        $('#modal-add').modal('hide');
                        notify(response);
                    },
                    error: function(error) {
                        $.each(error.responseJSON, function(key, value) {
                            $(`input[name="${key}"]`).addClass('is-invalid');
                            $(`.error-${key}`).removeClass('d-none').addClass(
                                'd-block').html(value);
                        })
                    }
                })
            });

            // On form update
            $('#form-update').submit(function(event) {
                event.preventDefault();
                let id = $(this).find('input[name="id"]').val();
                var formdata = new FormData(this);
                var form_element = $(this);

                $.ajax({
                    type: 'post',
                    url: "{{ url('kmp/fakultas') }}" + `/${id}`,
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        load_table_data();
                        clear_form(form_element);
                        notify(response);
                        $('#modal-update').modal('hide');
                    },
                    error: function(error) {
                        $.each(error.responseJSON, function(key, value) {
                            $(`input[name="${key}"]`).addClass('is-invalid');
                            $(`.error-${key}`).removeClass('d-none').addClass(
                                'd-block').html(value);
                        })
                    }
                })
            });

            // On form update
            $('#form-delete').submit(function(event) {
                event.preventDefault();
                let id = $(this).find('input[name="id"]').val();
                var formdata = new FormData(this);
                var form_element = $(this);

                $.ajax({
                    type: 'delete',
                    url: "{{ url('kmp/fakultas') }}" + `/${id}`,
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        load_table_data();
                        $('#modal-delete').modal('hide');
                        notify(response);
                    },
                    error: function(error) {
                        $.each(error.responseJSON, function(key, value) {
                            $(`input[name="${key}"]`).addClass('is-invalid');
                            $(`.error-${key}`).removeClass('d-none').addClass(
                                'd-block').html(value);
                        })
                    }
                })
            });

            // On add button click
            $(document).on('click', '.btn-add-data', function(e) {
                clear_form($('#form-submit'));
            });

            // On edit button click
            $(document).on('click', '.btn-edit-data', function(e) {
                // clear and open modal
                clear_form($('#form-update'));
                $('#modal-update').modal('show');

                let id = $(this).data('id');
                $.ajax({
                    url: "{{ url('kmp/fakultas') }}" + `/${id}`,
                    type: "GET",
                    cache: false,
                    success: function(response) {
                        //fill data to form
                        $('#form-update').find('input[name="id"]').val(response.id);
                        $('#form-update').find('input[name="kode"]').val(response.kode);
                        $('#form-update').find('input[name="nama"]').val(response.nama);
                    }
                });
            });

            // On add button click
            $(document).on('click', '.btn-delete-data', function(e) {
                let id = $(this).data('id');
                $('#modal-delete').modal('show');
                $('#modal-delete').find('input[name="id"]').val(id);
            });
        });

        function load_table_data() {
            $.ajax({
                type: "get",
                url: "{{ url('kmp/fakultas?option=load_table_data') }}",
                success: function(response) {
                    // destroy datatable
                    if ($.fn.DataTable.isDataTable('.datatable')) {
                        $('.datatable').DataTable().destroy();
                    }
                    // add data table row
                    $('.table-data').empty().html(response);
                    // reinitialize
                    create_datatable($('.datatable'));
                    // replacing placeholder
                    $('.content-placeholder').removeClass('d-block').addClass('d-none');
                    $('.content-real').removeClass('d-none').addClass('d-block');
                }
            });
        }

        function clear_form(element) {
            element.trigger('reset');
            element.find('.invalid-feedback').each(function() {
                $(this).removeClass('d-block').addClass(
                    'd-none');
            });
            element.find('input').each(function() {
                $(this).removeClass('is-invalid');
            });
        }
    </script>
@endpush
