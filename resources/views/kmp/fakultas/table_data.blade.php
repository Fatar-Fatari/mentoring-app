@foreach ($data as $index => $item)
    <tr>
        <td>{{ $index += 1 }}.</td>
        <td>{{ $item->kode }}</td>
        <td>{{ $item->nama }}</td>
        <td class="text-end">
            <button type="button" class="btn btn-edit-data" data-id="{{ $item->id }}">
                <i class="ti ti-edit me-2"></i>
                Edit
            </button>
            <button type="button" class="btn btn-delete-data" data-id="{{ $item->id }}">
                <i class="ti ti-trash me-2"></i>
                Delete
            </button>
        </td>
    </tr>
@endforeach
