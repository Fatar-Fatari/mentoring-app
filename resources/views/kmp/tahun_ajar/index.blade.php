@extends('layouts.app')
@section('page-title')
    <div>
        Master Data <i class="ti ti-chevron-right"></i> Tahun Ajar
    </div>
@endsection

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tahun Ajar Aktif</h3>
                <p class="card-subtitle">Data sistem bergantung dengan tahun ajaran aktif. Data dari tahun ajaran aktif akan
                    menjadi data utama untuk ditampilkan.</p>
                <select class="form-select select-tahunajar">
                </select>
            </div>
            <div class="card-footer">
                <div class="row align-items-center">
                    <div class="col">Simpan untuk atur ulang.</div>
                    <div class="col-auto">
                        <button class="btn btn-primary btn-update-active">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="w-100 d-flex align-items-center justify-content-between">
                    <h3 class="card-title">Data list</h3>
                    <button type="button" class="btn btn-primary d-none d-sm-inline-block btn-add-data"
                        data-bs-toggle="modal" data-bs-target="#modal-addTahunAjar">
                        <i class="ti ti-plus"></i>
                        Add new data
                    </button>
                </div>
            </div>
            <div class="content-placeholder d-block">
                <div class="row align-items-center m-4">
                    <div class="col-1">
                        <div class="avatar placeholder"></div>
                    </div>
                    <div class="col">
                        <div class="placeholder placeholder-xs col-9"></div>
                        <div class="placeholder placeholder-xs col-7"></div>
                    </div>
                    <div class="col">
                        <div class="placeholder placeholder-xs col-9"></div>
                        <div class="placeholder placeholder-xs col-7"></div>
                    </div>
                    <div class="col">
                        <div class="placeholder placeholder-xs col-9"></div>
                        <div class="placeholder placeholder-xs col-7"></div>
                    </div>
                    <div class="col ms-auto text-end">
                        <div class="placeholder placeholder-xs col-8"></div>
                        <div class="placeholder placeholder-xs col-10"></div>
                    </div>
                </div>
            </div>
            <div class="content-real d-none">
                <div class="card-body border-bottom py-3">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="text-muted datatable-length"></div>
                        <div class="ms-auto text-muted datatable-filter"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable">
                        <thead>
                            <tr>
                                <th class="w-1">No.</th>
                                <th>Tahun Ajar</th>
                                <th>Start</th>
                                <th>End</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="table-data">
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <div class="datatable-info"></div>
                    <div class="datatable-paginate"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-addTahunAjar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="javascript:void(0)" method="post" id="form-submit">
                    @csrf
                    @method('post')
                    <div class="modal-header">
                        <h5 class="modal-title">New data</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label class="form-label">Tahun Ajar</label>
                            <input type="text" class="form-control" name="tahun_ajar" placeholder="ex. 2022/2023">
                            <div class="invalid-feedback d-none error-tahun_ajar">Invalid feedback</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label class="form-label">Start</label>
                                    <div class="input-icon">
                                        <span class="input-icon-addon">
                                            <i class="ti ti-calendar-due"></i>
                                        </span>
                                        <input name="start" class="form-control litepicker" placeholder="Select a date"
                                            id="datepicker-icon" autocomplete="off">
                                    </div>
                                    <div class="invalid-feedback d-none error-start">Invalid feedback</div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label class="form-label">End</label>
                                    <div class="input-icon">
                                        <span class="input-icon-addon">
                                            <i class="ti ti-calendar-due"></i>
                                        </span>
                                        <input name="end" class="form-control litepicker" placeholder="Select a date"
                                            id="datepicker-icon" autocomplete="off">
                                    </div>
                                    <div class="invalid-feedback d-none error-end">Invalid feedback</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-primary ms-auto">
                            <i class="ti ti-plus me-1"></i>
                            Add new data
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-updateTahunAjar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="javascript:void(0)" method="post" id="form-update">
                    @csrf
                    @method('put')
                    <div class="modal-header">
                        <h5 class="modal-title">Edit data</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id">
                        <div class="mb-3">
                            <label class="form-label">Tahun Ajar</label>
                            <input type="text" class="form-control" name="tahun_ajar" placeholder="ex. 2022/2023">
                            <div class="invalid-feedback d-none error-tahun_ajar">Invalid feedback</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label class="form-label">Start</label>
                                    <div class="input-icon">
                                        <span class="input-icon-addon">
                                            <i class="ti ti-calendar-due"></i>
                                        </span>
                                        <input name="start" class="form-control litepicker" placeholder="Select a date"
                                            id="datepicker-icon" autocomplete="off">
                                    </div>
                                    <div class="invalid-feedback d-none error-start">Invalid feedback</div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label class="form-label">End</label>
                                    <div class="input-icon">
                                        <span class="input-icon-addon">
                                            <i class="ti ti-calendar-due"></i>
                                        </span>
                                        <input name="end" class="form-control litepicker" placeholder="Select a date"
                                            id="datepicker-icon" autocomplete="off">
                                    </div>
                                    <div class="invalid-feedback d-none error-end">Invalid feedback</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-primary ms-auto">
                            <i class="ti ti-refresh me-1"></i>
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-deleteTahunAjar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-status bg-danger"></div>
                <div class="modal-body text-center py-4">
                    <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-danger icon-lg" width="24"
                        height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                        stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M10.24 3.957l-8.422 14.06a1.989 1.989 0 0 0 1.7 2.983h16.845a1.989 1.989 0 0 0 1.7 -2.983l-8.423 -14.06a1.989 1.989 0 0 0 -3.4 0z" />
                        <path d="M12 9v4" />
                        <path d="M12 17h.01" />
                    </svg>
                    <h3>Are you sure?</h3>
                    <div class="text-muted">Do you really want to remove this data? What you've done cannot be undone.
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="w-100">
                        <div class="row">
                            <div class="col">
                                <button class="btn w-100" data-bs-dismiss="modal">
                                    Cancel
                                </button>
                            </div>
                            <div class="col">
                                <form action="javascript:void(0)"id="form-delete" method="post">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id">
                                    <button type="submit" class="btn btn-danger submit-text w-100">
                                        Delete 84 items
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('kmp.tahun_ajar.style_and_script')
