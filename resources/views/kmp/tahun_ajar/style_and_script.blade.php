@push('style')
    <style>
        .dataTables_length label,
        .dataTables_filter label {
            display: flex;
            gap: 8px;
        }

        .dataTables_paginate ul {
            margin: auto 0;
        }
    </style>
@endpush

@push('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        // Load table data from ajax request
        load_table_data();
        load_select_option();

        $(document).ready(function() {
            // On form submit
            $('#form-submit').submit(function(event) {
                event.preventDefault();
                var form_element = $(this);
                var formdata = new FormData(this);

                $.ajax({
                    type: 'post',
                    url: "{{ route('kmp.tahun_ajar.store') }}",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        load_table_data();
                        load_select_option();
                        clear_form(form_element);
                        $('#modal-addTahunAjar').modal('hide');
                        notify(response);
                    },
                    error: function(error) {
                        $.each(error.responseJSON, function(key, value) {
                            $(`input[name="${key}"]`).addClass('is-invalid');
                            $(`.error-${key}`).removeClass('d-none').addClass(
                                'd-block').html(value);
                        })
                    }
                })
            });

            // On form update
            $('#form-update').submit(function(event) {
                event.preventDefault();
                let id = $(this).find('input[name="id"]').val();
                var formdata = new FormData(this);
                var form_element = $(this);

                $.ajax({
                    type: 'post',
                    url: "{{ url('kmp/tahun_ajar') }}" + `/${id}`,
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        load_table_data();
                        load_select_option();
                        clear_form(form_element);
                        $('#modal-updateTahunAjar').modal('hide');
                        notify(response);
                    },
                    error: function(error) {
                        $.each(error.responseJSON, function(key, value) {
                            $(`input[name="${key}"]`).addClass('is-invalid');
                            $(`.error-${key}`).removeClass('d-none').addClass(
                                'd-block').html(value);
                        })
                    }
                })
            });

            // On form update
            $('#form-delete').submit(function(event) {
                event.preventDefault();
                let id = $(this).find('input[name="id"]').val();
                var formdata = new FormData(this);
                var form_element = $(this);

                $.ajax({
                    type: 'delete',
                    url: "{{ url('kmp/tahun_ajar') }}" + `/${id}`,
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        load_table_data();
                        load_select_option();
                        $('#modal-deleteTahunAjar').modal('hide');
                        notify(response);
                    },
                    error: function(error) {
                        $.each(error.responseJSON, function(key, value) {
                            $(`input[name="${key}"]`).addClass('is-invalid');
                            $(`.error-${key}`).removeClass('d-none').addClass(
                                'd-block').html(value);
                        })
                    }
                })
            });

            // On add button click
            $(document).on('click', '.btn-add-data', function(e) {
                clear_form($('#form-submit'));
            });

            // On edit button click
            $(document).on('click', '.btn-edit-data', function(e) {
                clear_form($('#form-update'));
                let index = $(this).data('index');
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ url('kmp/tahun_ajar') }}" + `/${id}`,
                    type: "GET",
                    cache: false,
                    success: function(response) {
                        //fill data to form
                        $('#form-update').find('input[name="id"]').val(response.id);
                        $('#form-update').find('input[name="tahun_ajar"]').val(response
                            .tahun_ajar);
                        $('#form-update').find('input[name="start"]').val(response.start);
                        $('#form-update').find('input[name="end"]').val(response.end);

                        //open modal
                        $('#modal-updateTahunAjar').modal('show');
                        $('#modal-updateTahunAjar').find('.modal-title').html(
                            `Edit data: #${index}`);
                    }
                });
            });

            // On add button click
            $(document).on('click', '.btn-delete-data', function(e) {
                let index = $(this).data('index');
                let id = $(this).data('id');
                $('#modal-deleteTahunAjar').modal('show');
                $('#modal-deleteTahunAjar').find('input[name="id"]').val(id);
                $('#modal-deleteTahunAjar').find('.submit-text').html(`Delete data #${index}`);
            });

            // On update-active button click
            $(document).on('click', '.btn-update-active', function(e) {
                var id = $('.select-tahunajar').val();
                $.ajax({
                    url: "{{ route('kmp.tahun_ajar_update_active') }}",
                    type: "post",
                    cache: false,
                    data: {
                        id: id
                    },
                    success: function(response) {
                        load_table_data();
                        load_select_option();
                        notify(response);
                    }
                });
            });
        });

        function load_table_data() {
            $.ajax({
                type: "get",
                url: "{{ url('kmp/tahun_ajar?option=load_table_data') }}",
                success: function(response) {
                    // destroy datatable
                    if ($.fn.DataTable.isDataTable('.datatable')) {
                        $('.datatable').DataTable().destroy();
                    }
                    // add data table row
                    $('.table-data').empty().html(response);
                    // reinitialize
                    create_datatable($('.datatable'));
                    // replacing placeholder
                    $('.content-placeholder').removeClass('d-block').addClass('d-none');
                    $('.content-real').removeClass('d-none').addClass('d-block');
                }
            });
        }

        function load_select_option() {
            $.ajax({
                type: "get",
                url: "{{ url('kmp/tahun_ajar?option=load_select_option') }}",
                success: function(response) {
                    $('.select-tahunajar').empty();
                    $.each(response, function(key, value) {
                        $('.select-tahunajar')
                            .append(`<option value="${value.id}"` + (value.active == true ? 'selected' :
                                '') + `>${value.tahun_ajar}</option>`);
                    })
                }
            });
        }

        function clear_form(element) {
            element.trigger('reset');
            element.find('.invalid-feedback').each(function() {
                $(this).removeClass('d-block').addClass(
                    'd-none');
            });
            element.find('input').each(function() {
                $(this).removeClass('is-invalid');
            });
        }
    </script>
@endpush
