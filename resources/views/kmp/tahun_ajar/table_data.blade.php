@foreach ($data as $index => $item)
    <tr>
        <td>{{ $index += 1 }}.</td>
        <td>
            {{ $item->tahun_ajar }}
            @if ($item->active)
                <span class="badge bg-green-lt">Active</span>
            @endif
        </td>
        <td>{{ \Carbon\Carbon::parse($item->start)->format('d M Y') }}</td>
        <td>{{ \Carbon\Carbon::parse($item->end)->format('d M Y') }}</td>
        <td class="text-end">
            <button type="button" class="btn btn-edit-data" data-id="{{ $item->id }}" data-index="{{ $index }}">
                <i class="ti ti-edit me-2"></i>
                Edit
            </button>
            <button type="button" class="btn btn-delete-data" data-id="{{ $item->id }}"
                data-index="{{ $index }}">
                <i class="ti ti-trash me-2"></i>
                Delete
            </button>
        </td>
    </tr>
@endforeach
