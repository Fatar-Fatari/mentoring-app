@extends('layouts.app')
@section('page-title')
    <div>
        Materi Kelas
    </div>
@endsection

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div>
                    <h3 class="card-title">Materi kelas mentoring</h3>
                    <p class="card-subtitle">
                        Pelajari dan ajarkan materi mentoring kepada para mentee dengan sebaik-baiknya.
                    </p>
                </div>
            </div>
            <div class="content-data">
                <div class="row align-items-center m-4">
                    <div class="col-2">
                        <div class="avatar placeholder"></div>
                    </div>
                    <div class="col">
                        <div class="placeholder placeholder-xs col-9"></div>
                        <div class="placeholder placeholder-xs col-7"></div>
                    </div>
                    <div class="col">
                        <div class="placeholder placeholder-xs col-9"></div>
                        <div class="placeholder placeholder-xs col-7"></div>
                    </div>
                    <div class="col ms-auto text-end">
                        <div class="placeholder placeholder-xs col-8"></div>
                        <div class="placeholder placeholder-xs col-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('mentor.materi.style_and_script')
