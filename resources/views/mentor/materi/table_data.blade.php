<div class="card-body border-bottom py-3">
    <div class="d-flex align-items-center justify-content-between">
        <div class="text-muted datatable-length"></div>
        <div class="ms-auto text-muted datatable-filter"></div>
    </div>
</div>
<div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap datatable">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama Kelas</th>
                <th>File</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $index => $item)
                <tr>
                    <td></td>
                    <td>{{ $item->nama }}</td>
                    <td>
                        <a href="{{ asset('file-uploads/materi') . '/' . $item->file }}"
                            class="btn border-blue bg-blue-lt" download>
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-file-download"
                                width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M14 3v4a1 1 0 0 0 1 1h4"></path>
                                <path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z"></path>
                                <path d="M12 17v-6"></path>
                                <path d="M9.5 14.5l2.5 2.5l2.5 -2.5"></path>
                            </svg>
                            <span class="text">Download</span>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="card-footer d-flex align-items-center justify-content-between">
    <div class="datatable-info"></div>
    <div class="datatable-paginate"></div>
</div>
