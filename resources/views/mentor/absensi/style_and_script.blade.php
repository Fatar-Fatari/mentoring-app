@push('script')
    <script>
        $(document).ready(function() {
            // Pertemuan submit
            $('#form-pertemuan').submit(function(e) {
                e.preventDefault();
                let pertemuan = $(this).find('select[name="pertemuan"]').val();
                $('.content-data').empty().html(`
                    <div class="col p-4">
                    <div class="placeholder placeholder-xs col-9"></div>
                    <div class="placeholder placeholder-xs col-7"></div>
                    <div class="placeholder placeholder-xs col-4"></div>
                    </div>
                `);
                loadDataTable("{{ url('mentor/absensi?option=load_table_data') }}" +
                    `&pertemuan=${pertemuan}`);
            });

            // Upload bukti
            $(document).on('submit', '#form-bukti', function(e) {
                e.preventDefault();
                let formdata = new FormData(this);

                $.ajax({
                    url: "{{ route('mentor.absensi.store') }}",
                    type: 'post',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        notify(response);
                        $('#form-pertemuan').trigger('submit');
                    }
                })
            });

            // Save kehadiran
            $(document).on('submit', '#form-kehadiran', function(e) {
                e.preventDefault();
                let formdata = new FormData(this);

                $.ajax({
                    url: "{{ route('mentor.absensi.store') }}",
                    type: 'post',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        notify(response);
                        $('#form-pertemuan').trigger('submit');
                    }
                })
            });
        });
    </script>
@endpush
