<div class="card-body">
    <div class="accordion" id="accordion-example">
        <div class="accordion-item">
            <h2 class="accordion-header" id="heading-1">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapse-1" aria-expanded="false">
                    @if ($bukti->file == null)
                        <span class="badge bg-red-lt">Belum upload bukti</span>
                    @else
                        Bukti Kegiatan
                    @endif
                </button>
            </h2>
            <div id="collapse-1" class="accordion-collapse collapse" data-bs-parent="#accordion-example" style="">
                <div class="accordion-body pt-0">
                    @if ($bukti->file != null)
                        <img src="{{ asset('file-uploads/bukti_kegiatan') . '/' . $bukti->file }}"
                            class="img-fluid mx-auto border mb-3" alt="">
                    @endif
                    <form action="javascript:void(0)" id="form-bukti" method="post">
                        @csrf
                        @method('post')
                        @if ($bukti->file != null)
                            <span class="text-muted mb-1">Upload ulang untuk mengganti file.</span>
                        @endif
                        <div class="d-flex align-items-center gap-2">
                            <input type="hidden" name="bukti_id" value="{{ $bukti->id }}">
                            <input type="file" name="file" id="" class="form-control" required>
                            <button class="btn btn-primary">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<form id="form-kehadiran" action="javascript:void(0)" method="post">
    @csrf
    @method('post')
    <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Mentee</th>
                    <th>Kehadiran</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $index => $item)
                    <tr>
                        <td>{{ $index += 1 }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>
                            <select name="kehadiran[]"
                                class="form-select {{ $item->kehadiran == true ? 'is-valid' : 'is-invalid' }}">
                                <option value="{{ $item->absensi . '-' . '1' }}"
                                    {{ $item->kehadiran == true ? 'selected' : '' }}>Hadir
                                </option>
                                <option value="{{ $item->absensi . '-' . '0' }}"
                                    {{ $item->kehadiran == false ? 'selected' : '' }}>Tidak hadir</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>
