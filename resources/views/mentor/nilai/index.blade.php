@extends('layouts.app')
@section('page-title', 'Nilai')

@section('content')
    @if (!Auth::user()->kelas->isEmpty())
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="mb-4">Info Kelas</h2>
                    <div class="row g-3">
                        <div class="col-md">
                            <div class="form-label">Nama Kelas</div>
                            <div class="border rounded py-2 px-3">
                                {{ $kelas->nama }}
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-label">PJ Mentor</div>
                            <div class="border rounded py-2 px-3">
                                {{ $kelas->mentor->nama }}
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-label">Total Mentee</div>
                            <div class="border rounded py-2 px-3">
                                {{ $kelas->mentee->count() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Absensi</h3>
                    <div class="card-actions">
                        <form action="javascript:void(0)" method="post" id="form-pertemuan">
                            <div class="input-group">
                                @csrf
                                @method('post')
                                <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
                                <select name="pertemuan" class="form-select">
                                    @for ($i = 1; $i <= 14; $i++)
                                        <option value="{{ $i }}">Pertemuan {{ $i }}</option>
                                    @endfor
                                </select>
                                <button type="submit" class="btn btn-primary">Go</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="content-data">
                    <div class="card-body">
                        <div class="alert alert-secondary">
                            <h4 class="alert-title">Pilih pertemuan!</h4>
                            <div class="text-secondary">Data akan ditampilkan sesuai dengan pertemuan yg dipilih!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col">
            <div class="alert alert-info w-100" role="alert">
                <h4 class="alert-title">Oops!</h4>
                <div class="text-secondary">Anda tidak memiliki kelas aktif!</div>
            </div>
        </div>
    @endif
@endsection

@include('mentor.nilai.style_and_script')
