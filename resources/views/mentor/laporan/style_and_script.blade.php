@push('script')
    <script>
        $(document).ready(function() {
            // Pertemuan submit
            $('#form-pertemuan').submit(function(e) {
                e.preventDefault();
                let pertemuan = $(this).find('select[name="pertemuan"]').val();
                $('.content-data').empty().html(`
                    <div class="col p-4">
                    <div class="placeholder placeholder-xs col-9"></div>
                    <div class="placeholder placeholder-xs col-7"></div>
                    <div class="placeholder placeholder-xs col-4"></div>
                    </div>
                `);
                loadDataTable("{{ url('mentor/nilai?option=load_table_data') }}" +
                    `&pertemuan=${pertemuan}`);
            });

            // Save nilai
            $(document).on('submit', '#form-nilai', function(e) {
                e.preventDefault();
                let formdata = new FormData(this);

                $.ajax({
                    url: "{{ route('mentor.nilai.store') }}",
                    type: 'post',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        notify(response);
                        $('#form-pertemuan').trigger('submit');
                    }
                })
            });

            $(document).on('blur', 'input[name="nilai"]', function(e) {
                var value = $(this).val();
                if (value > 100) {
                    $(this).val(100);
                }
            });
        });
    </script>
@endpush
