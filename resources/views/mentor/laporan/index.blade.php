@extends('layouts.app')
@section('page-title', 'Laporan')
@section('title-buttons')
    <div class="col-auto ms-auto d-print-none">
        <button type="button" class="btn btn-primary" onclick="javascript:window.print();">
            <!-- Download SVG icon from http://tabler-icons.io/i/printer -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2"></path>
                <path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4"></path>
                <path d="M7 13m0 2a2 2 0 0 1 2 -2h6a2 2 0 0 1 2 2v4a2 2 0 0 1 -2 2h-6a2 2 0 0 1 -2 -2z"></path>
            </svg>
            Print
        </button>
    </div>
@endsection

@push('style')
    <style>
        .nav-link.active {
            background-color: #0054a6 !important;
            color: #fff !important;
            font-weight: 700;
        }
    </style>
@endpush

@section('content')
    <div class="card d-print-none">
        <div class="card-body">
            <ul class="nav nav-pills card-header-pills nav-fill m-0" data-bs-toggle="tabs">
                <li class="nav-item">
                    <a href="#tab-absensi" class="nav-link active" data-bs-toggle="tab">
                        Absensi
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#tab-nilai" class="nav-link" data-bs-toggle="tab">
                        Nilai
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="card py-2">
        <div class="card-header">
            <div class="row g-3 w-100">
                <div class="col-md">
                    <div class="form-label">Nama Kelas</div>
                    <div class="border rounded py-2 px-3">
                        {{ $kelas->nama }}
                    </div>
                </div>
                <div class="col-md">
                    <div class="form-label">PJ Mentor</div>
                    <div class="border rounded py-2 px-3">
                        {{ $kelas->mentor->nama }}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab-absensi">
                    <div class="text-center">
                        <h4 class="card-title m-0 my-3">Absensi Kelas</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-vcenter table-bordered">
                            <thead>
                                <tr>
                                    <th>Nim</th>
                                    <th>Nama</th>
                                    @foreach ($data[0]->absensi as $item)
                                        <th>{{ $item->pertemuan }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->nim }}</td>
                                        <td>{{ $item->nama }}</td>
                                        @foreach ($item->absensi as $item)
                                            <td>
                                                @if ($item->kehadiran == true)
                                                    <span class="badge bg-green-lt border-green"><i
                                                            class="ti ti-check"></i></span>
                                                @else
                                                    <span class="badge bg-red-lt border-red"><i class="ti ti-x"></i></span>
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tab-nilai">
                    <div class="text-center">
                        <h4 class="card-title m-0 my-3">Penilaian Kelas</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-vcenter table-bordered">
                            <thead>
                                <tr>
                                    <th>Nim</th>
                                    <th>Nama</th>
                                    @foreach ($data[0]->nilai as $item)
                                        <th>{{ $item->pertemuan }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->nim }}</td>
                                        <td>{{ $item->nama }}</td>
                                        @foreach ($item->nilai as $item)
                                            <td>
                                                <b>{{ $item->nilai }}</b>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

{{-- @include('mentor.nilai.style_and_script') --}}
