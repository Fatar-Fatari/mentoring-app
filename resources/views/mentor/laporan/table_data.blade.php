<form id="form-nilai" action="javascript:void(0)" method="post">
    @csrf
    @method('post')
    <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Mentee</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $index => $item)
                    <tr>
                        <td>{{ $index += 1 }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>
                            <input type="hidden" name="nilai_id[]" value="{{ $item->nilai_id }}">
                            <input type="number" name="nilai[]" class="form-control" min="0" max="100"
                                value="{{ $item->nilai }}">
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>
