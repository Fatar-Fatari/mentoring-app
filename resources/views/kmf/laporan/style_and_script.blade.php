@push('script')
    <script>
        $(document).ready(function() {
            // filter generate
            $(document).on('submit', '#form-filter', function(e) {
                e.preventDefault();
                let formdata = new FormData(this);

                $.ajax({
                    url: "{{ route('kmf.laporan.filter') }}",
                    type: 'post',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        $('.content-data').empty().html(response);
                    }
                })
            });
        });
    </script>
@endpush
