@extends('layouts.app')
@section('page-title', 'Laporan')
@section('title-buttons')
    <div class="col-auto ms-auto d-print-none">
        <button type="button" class="btn btn-primary" onclick="javascript:window.print();">
            <!-- Download SVG icon from http://tabler-icons.io/i/printer -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2"></path>
                <path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4"></path>
                <path d="M7 13m0 2a2 2 0 0 1 2 -2h6a2 2 0 0 1 2 2v4a2 2 0 0 1 -2 2h-6a2 2 0 0 1 -2 -2z"></path>
            </svg>
            Print
        </button>
    </div>
@endsection

@section('content')
    <div class="card d-print-none">
        <div class="card-body">
            <form id="form-filter" action="javascript:void(0)" method="post">
                <div class="row g-3 w-100">
                    @csrf
                    @method('post')
                    <div class="col-md">
                        <label class="form-label">Kelas</label>
                        <select name="kelas_id" class="form-select">
                            @foreach ($fakultas->kelas as $item)
                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md">
                        <label class="form-label">Jenis</label>
                        <select name="jenis" class="form-select">
                            <option value="absensi">Absensi</option>
                            <option value="nilai">Nilai</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="form-label">Action</label>
                        <button type="submit" class="btn btn-primary w-100">Generate</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="content-data"> </div>

@endsection

@include('kmf.laporan.style_and_script')
