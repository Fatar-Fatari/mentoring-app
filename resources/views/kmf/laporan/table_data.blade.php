<div class="card py-2">
    <div class="card-header">
        <div class="row g-3 w-100">
            <div class="col-md">
                <div class="form-label">Nama Kelas</div>
                <div class="border rounded py-2 px-3">
                    {{ $kelas->nama }}
                </div>
            </div>
            <div class="col-md">
                <div class="form-label">PJ Mentor</div>
                <div class="border rounded py-2 px-3">
                    {{ $kelas->mentor->nama }}
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="text-center">
            <h4 class="card-title m-0 my-3">{{ $jenis == 'absensi' ? 'Absensi Kelas' : 'Penilaian Kelas' }}</h4>
        </div>
        <div class="table-responsive">
            <table class="table table-vcenter table-bordered">
                <thead>
                    <tr>
                        <th>Nim</th>
                        <th>Nama</th>
                        @if ($jenis == 'absensi')
                            @foreach ($data[0]->absensi as $item)
                                <th>{{ $item->pertemuan }}</th>
                            @endforeach
                        @elseif ($jenis == 'nilai')
                            @foreach ($data[0]->nilai as $item)
                                <th>{{ $item->pertemuan }}</th>
                            @endforeach
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->nim }}</td>
                            <td>{{ $item->nama }}</td>
                            @if ($jenis == 'absensi')
                                @foreach ($item->absensi as $item)
                                    <td>
                                        @if ($item->kehadiran == true)
                                            <span class="badge bg-green-lt border-green"><i
                                                    class="ti ti-check"></i></span>
                                        @else
                                            <span class="badge bg-red-lt border-red"><i class="ti ti-x"></i></span>
                                        @endif
                                    </td>
                                @endforeach
                            @elseif ($jenis == 'nilai')
                                @foreach ($item->nilai as $item)
                                    <td>
                                        <b>{{ $item->nilai }}</b>
                                    </td>
                                @endforeach
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
