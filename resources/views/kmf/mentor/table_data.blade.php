<div class="card-body border-bottom py-3">
    <div class="d-flex align-items-center justify-content-between">
        <div class="text-muted datatable-length"></div>
        <div class="ms-auto text-muted datatable-filter"></div>
    </div>
</div>
<div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap datatable">
        <thead>
            <tr>
                <th class="w-1">No.</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $index => $item)
                <tr>
                    <td></td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->username }}</td>
                    <td>{{ $item->email }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="card-footer d-flex align-items-center justify-content-between">
    <div class="datatable-info"></div>
    <div class="datatable-paginate"></div>
</div>
