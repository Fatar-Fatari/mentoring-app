@extends('layouts.app')
@section('page-title', 'Home')

@section('content')
    <div class="card">
        <div class="card-header">Welcome</div>
        <div class="card-body">
            <h4>{{ Auth::user()->nama }}</h4>
        </div>
    </div>
@endsection
