@extends('layouts.app')
@section('page-title')
    <div>
        Kelas
    </div>
@endsection

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="w-100 d-flex align-items-center justify-content-between">
                    <div>
                        <h3 class="mb-1">{{ $kelas->fakultas->kode . ' - ' . $kelas->tahun_ajar->tahun_ajar }}</h3>
                        <h3 class="card-title">{{ $kelas->nama }}</h3>
                    </div>
                    <button type="button" class="btn btn-primary d-none d-sm-inline-block btn-add-data" data-bs-toggle="modal"
                        data-bs-target="#modal-add">
                        <i class="ti ti-plus"></i>
                        Add mentee
                    </button>
                </div>
            </div>
            <div class="content-data">
                <div class="row align-items-center m-4">
                    <div class="col-2">
                        <div class="avatar placeholder"></div>
                    </div>
                    <div class="col">
                        <div class="placeholder placeholder-xs col-9"></div>
                        <div class="placeholder placeholder-xs col-7"></div>
                    </div>
                    <div class="col">
                        <div class="placeholder placeholder-xs col-9"></div>
                        <div class="placeholder placeholder-xs col-7"></div>
                    </div>
                    <div class="col ms-auto text-end">
                        <div class="placeholder placeholder-xs col-8"></div>
                        <div class="placeholder placeholder-xs col-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-add" tabindex="-1" role="dialog" aria-hidden="true" data-focus="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="javascript:void(0)" method="post" id="form-submit">
                    @csrf
                    @method('post')
                    <div class="modal-header">
                        <h5 class="modal-title">Add mentee</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
                            <label class="form-label">Mentee</label>
                            <select name="mentee_id" class="form-select mentee-select" data-placeholder="Select mentee">
                                <option></option>
                                {{-- @foreach ($mentee as $item)
                                    <option value="{{ $item->id }}">{{ $item->nim . ' - ' . $item->nama }}</option>
                                @endforeach --}}
                            </select>
                            <div class="invalid-feedback d-none error-mentee_id">Invalid feedback</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-primary ms-auto">
                            <i class="ti ti-plus me-1"></i>
                            Add mentee
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-delete" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-status bg-danger"></div>
                <div class="modal-body text-center py-4">
                    <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-danger icon-lg" width="24"
                        height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                        stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M10.24 3.957l-8.422 14.06a1.989 1.989 0 0 0 1.7 2.983h16.845a1.989 1.989 0 0 0 1.7 -2.983l-8.423 -14.06a1.989 1.989 0 0 0 -3.4 0z" />
                        <path d="M12 9v4" />
                        <path d="M12 17h.01" />
                    </svg>
                    <h3>Are you sure?</h3>
                    <div class="text-muted">Do you really want to remove this data? What you've done cannot be undone.
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="w-100">
                        <div class="row">
                            <div class="col">
                                <button class="btn w-100" data-bs-dismiss="modal">
                                    Cancel
                                </button>
                            </div>
                            <div class="col">
                                <form action="javascript:void(0)"id="form-delete" method="post">
                                    @csrf
                                    @method('post')
                                    <input type="hidden" name="mentee_id">
                                    <button type="submit" class="btn btn-danger submit-text w-100">
                                        Delete
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
    <style>
        .dataTables_length label,
        .dataTables_filter label {
            display: flex;
            gap: 8px;
        }

        .dataTables_paginate ul {
            margin: auto 0;
        }

        .select2-results__option {
            font-size: 30px;
        }
    </style>
@endpush

@push('script')
    <script>
        // Load table data from ajax request
        loadDataTable("{{ route('kmf.kelas.edit', $kelas->id) . '?option=load_table_data' }}");
        load_mentee();

        $(document).ready(function() {
            // On form submit
            handleFormSubmitUpdate(
                $('#form-submit'), "{{ route('kmf.add_mentee_to_kelas') }}",
                function(response) {
                    notify(response);
                    loadDataTable("{{ route('kmf.kelas.edit', $kelas->id) . '?option=load_table_data' }}");
                    clearForm($('#form-submit'));
                    load_mentee();
                },
                function(error) {
                    handleFormErrors(error, ['input', 'textarea', 'select']);
                });

            // On form update
            handleFormSubmitUpdate(
                $('#form-delete'), "{{ route('kmf.remove_mentee_from_kelas') }}",
                function(response) {
                    loadDataTable("{{ route('kmf.kelas.edit', $kelas->id) . '?option=load_table_data' }}");
                    $('#modal-delete').modal('hide');
                    notify(response);
                    load_mentee();
                });

            $(document).on('click', '.btn-add-data', function(e) {
                $('.mentee-select').val(null).change();
            });

            // On delete button click
            $(document).on('click', '.btn-delete-data', function(e) {
                let id = $(this).data('id');
                $('#modal-delete').modal('show');
                $('#modal-delete').find('input[name="mentee_id"]').val(id);
            });

            $('.mentee-select').each(function() {
                $(this).select2({
                    theme: 'bootstrap-5',
                    dropdownParent: $(this).closest('.modal'),
                });
            });
        });

        function load_mentee(params) {
            $.ajax({
                type: 'get',
                url: "{{ route('kmf.kelas.edit', $kelas->id) . '?option=load_mentee' }}",
                success: function(response) {
                    $('.mentee-select').find('option').not(':first').remove();
                    $.each(response, function(i, item) {
                        $('.mentee-select').append($('<option>', {
                            value: item.id,
                            text: item.nim + ' - ' + item.nama
                        }));
                    });
                }
            })
        }
    </script>
@endpush
