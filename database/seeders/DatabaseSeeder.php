<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Fakultas;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // DB tahun ajar seeder
        DB::table('tahun_ajars')->insert([
            'tahun_ajar' => '2022/2023',
            'start' => Carbon::parse('2022-10-10'),
            'end' => Carbon::parse('2023-10-10'),
            'active' => true,
        ]);

        $fakultas = [
            [
                'kode' => 'FAI',
                'nama' => 'Fakultas Agama Islam',
            ],
            [
                'kode' => 'FEB',
                'nama' => 'Fakultas Ekonomi dan Bisnis',
            ],
            [
                'kode' => 'FF',
                'nama' => 'Fakultas Farmasi',
            ],
            [
                'kode' => 'FG',
                'nama' => 'Fakultas Geografi',
            ], [
                'kode' => 'FH',
                'nama' => 'Fakultas Hukum',
            ], [
                'kode' => 'FIK',
                'nama' => 'Fakultas Ilmu Kesehatan',
            ], [
                'kode' => 'FK',
                'nama' => 'Fakultas Kedokteran',
            ],
            [
                'kode' => 'FKG',
                'nama' => 'Fakultas Kedokteran Gigi',
            ], [
                'kode' => 'FKI',
                'nama' => 'Fakultas Komunikasi dan Informatika',
            ], [
                'kode' => 'FKIP',
                'nama' => 'Fakultas Keguruan dan Ilmu Pendidikan',
            ],
            [
                'kode' => 'FP',
                'nama' => 'Fakultas Psikologi',
            ],
            [
                'kode' => 'FT',
                'nama' => 'Fakultas Teknik',
            ]
        ];
        // DB fakultas seeder
        foreach ($fakultas as $fakultas) {
            DB::table('fakultas')->insert($fakultas);
        }

        // DB user seeder
        $users = [
            [
                'nama' => 'Mentor',
                'username' => 'mentor',
                'email' => 'mentor@mail.test',
                'password' => bcrypt('password'),
                'role' => 'mentor',
                'fakultas_id' => 1
            ],
            [
                'nama' => 'Koordinator Fakultas',
                'username' => 'kmf',
                'email' => 'kmf@mail.test',
                'password' => bcrypt('password'),
                'role' => 'kmf',
                'fakultas_id' => 1
            ],
            [
                'nama' => 'Koordinator Pusat',
                'username' => 'kmp',
                'email' => 'kmp@mail.test',
                'password' => bcrypt('password'),
                'role' => 'kmp',
            ],
        ];
        foreach ($users as $key => $user) {
            User::create($user);
        }
    }
}
