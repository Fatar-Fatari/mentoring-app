<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kelas_id')
                ->constrained(
                    table: 'kelas',
                    indexName: 'nilais_kelas_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('mentee_id')
                ->constrained(
                    table: 'mentees',
                    indexName: 'nilais_mentee_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->integer('pertemuan');
            $table->integer('nilai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('nilais');
    }
};
