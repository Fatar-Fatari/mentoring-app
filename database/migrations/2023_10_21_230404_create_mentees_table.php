<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mentees', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('nim')->unique();
            $table->enum('gender', ['L', 'P']);
            $table->string('no_hp')->unique();
            $table->foreignId('tahun_ajar_id')
                ->constrained(
                    table: 'tahun_ajars',
                    indexName: 'mentees_tahun_ajar_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('fakultas_id')
                ->constrained(
                    table: 'fakultas',
                    indexName: 'mentees_fakultas_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('kelas_id')
                ->nullable()
                ->constrained(
                    table: 'kelas',
                    indexName: 'mentees_kelas_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mentees');
    }
};
