<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tahun_ajar_id')
                ->constrained(
                    table: 'tahun_ajars',
                    indexName: 'kelas_tahun_ajar_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('fakultas_id')
                ->constrained(
                    table: 'fakultas',
                    indexName: 'kelas_fakultas_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('mentor_id')
                ->constrained(
                    table: 'users',
                    indexName: 'kelas_mentor_id'
                )
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('nama');
            $table->enum('tipe', ['akhwat', 'ikhwan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kelas');
    }
};
